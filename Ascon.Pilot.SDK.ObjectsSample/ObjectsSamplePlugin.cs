﻿using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Media;
using Ascon.Pilot.Theme.ColorScheme;

namespace Ascon.Pilot.SDK.ObjectsSample
{
    [Export(typeof(INewTabPage))]
    public class ObjectsSamplePlugin : INewTabPage
    {
        private readonly IObjectsRepository _repository;
        private readonly ITabServiceProvider _tabServiceProvider;
        private readonly IPilotDialogService _pilotDialogService;
        private readonly IObjectModifier _modifier;
        private readonly IConnectionStateProvider _connectionStateProvider;
        private readonly IAttributeFormatParser _attributeFormatParser;

        [ImportingConstructor]
        public ObjectsSamplePlugin(
            IObjectsRepository repository, 
            ITabServiceProvider tabServiceProvider, 
            IPilotDialogService pilotDialogService, 
            IAttributeFormatParser attributeFormatParser,
            IObjectModifier modifier,
            IConnectionStateProvider connectionStateProvider)
        {
            _repository = repository;
            _tabServiceProvider = tabServiceProvider;
            _pilotDialogService = pilotDialogService;
            _modifier = modifier;
            _connectionStateProvider = connectionStateProvider;
            _attributeFormatParser = attributeFormatParser;

            var convertFromString = ColorConverter.ConvertFromString(pilotDialogService.AccentColor);
            if (convertFromString != null)
            {
                var accentColor = (Color)convertFromString;
                ColorScheme.Initialize(accentColor, pilotDialogService.Theme);
            }
        }

        public void BuildNewTabPage(INewTabPageHost host)
        {
            host.AddButton("Objects plugin sample", "OpenExtensionCommand", "Objects plugin sample", null);
        }

        public void OnButtonClick(string name)
        {
            if (name != "OpenExtensionCommand") 
                return;
            
            var title = _tabServiceProvider.GetActiveTabPageTitle();
            _tabServiceProvider.UpdateTabPageContent(title, "Objects plugin sample", GetMainView());
        }

        private FrameworkElement GetMainView()
        {
            var view = new MainView();
            var viewModel = new MainViewModel(_repository, _pilotDialogService, _tabServiceProvider, _attributeFormatParser, _modifier, _connectionStateProvider);
            view.DataContext = viewModel;
            return view;
        }
    }
}
