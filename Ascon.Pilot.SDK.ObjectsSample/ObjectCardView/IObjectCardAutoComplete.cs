﻿namespace Ascon.Pilot.SDK.ObjectsSample.ObjectCardView
{
    public interface IObjectCardAutoComplete
    {
        void Fill(string attributeName, object value);
    }
}
