﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Data;
using System.Windows.Markup;
using Ascon.Pilot.Theme.Tools;

namespace Ascon.Pilot.SDK.ObjectsSample.ObjectCardView
{
    public class NumeratorValueConverter : BaseValueConverter
    {
        protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var nodeValue = value as NumeratorInfoWrapper;
            return nodeValue == null ? null : nodeValue.Configuration;
        }

        protected override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var nodeValue = value as NumeratorInfoWrapper;
            return nodeValue == null ? null : nodeValue.Configuration;
        }
    }

    public class NumeratorConfigurationConverter : MarkupExtension, IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var configuration = values[0] as string;
            var parser = values[1] as IAttributeFormatParser;
            if (configuration == null || parser == null)
                return new List<INumeratorInfo>();

            IEnumerable<INumeratorInfo> collection;
            parser.TryParseNumeratorDeclaration(configuration, out collection);
            return collection.Select(x => new NumeratorInfoWrapper(x)).ToList();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class NumeratorInfoWrapper
    {
        public string DisplayName { get; private set; }
        public string Configuration { get; private set; }

        public NumeratorInfoWrapper(INumeratorInfo numeratorInfo)
        {
            DisplayName = numeratorInfo.DisplayName;
            Configuration = numeratorInfo.Configuration;
        }
    }
}
