﻿using System;
using System.Windows;
using System.Windows.Controls;
namespace Ascon.Pilot.SDK.ObjectsSample.ObjectCardView
{
    /// <summary>
    /// Interaction logic for ObjectPropertiesView.xaml
    /// </summary>
    public partial class ObjectPropertiesView : UserControl
    {
        public ObjectPropertiesView()
        {
            InitializeComponent();
            DataContextChanged += OnDataContextChanged;
        }

        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldViewModel = e.OldValue as ObjectCardViewModel;
            if (oldViewModel != null)
                oldViewModel.IsValidInputChanged -= IsValidInputChanged;

            var newViewMolde = e.NewValue as ObjectCardViewModel;
            if (newViewMolde != null)
                newViewMolde.IsValidInputChanged += IsValidInputChanged;
        }

        private void IsValidInputChanged(object sender, EventArgs e)
        {
            btnOk.IsEnabled = ViewModel.IsValidInput;
        }

        private ObjectCardViewModel ViewModel
        {
            get { return DataContext as ObjectCardViewModel; }
        }

        private void OnOkButtonClick(object sender, RoutedEventArgs e)
        {
            ViewModel.Save();
            CloseParentWindow();
        }

        private void OnCancelButtonClick(object sender, RoutedEventArgs e)
        {
            CloseParentWindow();
        }

        private void CloseParentWindow()
        {
            var parentWindow = Parent as Window;
            if(parentWindow !=null)
                parentWindow.Close();
        }
    }
}
