﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows.Controls;

namespace Ascon.Pilot.SDK.ObjectsSample.ObjectCardView.ValidationRules
{
    internal class DoubleOnlyValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object oValue, CultureInfo cultureInfo)
        {
            var value = oValue as string;

            if (String.IsNullOrEmpty(value))
                return ValidationResult.ValidResult;

            if (!IsValid(value))
                return new ValidationResult(false, "Please input a correct double");

            return ValidationResult.ValidResult;
        }

        public static bool IsValid(string value)
        {
            double d;
            return Double.TryParse(value, NumberStyles.Number, Thread.CurrentThread.CurrentUICulture, out d);
        }
    }
}
