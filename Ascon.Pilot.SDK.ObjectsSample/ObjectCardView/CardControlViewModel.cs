﻿using System;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Input;
using Ascon.Pilot.Theme.Tools;
using Microsoft.Practices.Prism.Commands;

namespace Ascon.Pilot.SDK.ObjectsSample.ObjectCardView
{
    class CardControlViewModel : PropertyChangedBase, ISingleValidationErrorHandler
    {
        private TypeWrapper _type;
        private readonly IPilotDialogService _dialogService;
        private readonly IObjectsRepository _repository;
        private readonly IAttributeFormatParser _attributeFormatParser;
        private IObjectCardAutoComplete _autoComplete;
        private object _value;
        private bool _hasValidationError;
        private bool _isReadOnly;
        private readonly DValue _originalValue;
        private string _format;
        private string _culture;
        private bool _editMode;

        public CardControlViewModel(AttributeWrapper attribute, TypeWrapper type, object initValue, bool isReadOnly, IPilotDialogService dialogService, IObjectsRepository repository, IObjectCardAutoComplete autoComplete, IAttributeFormatParser attributeFormatParser, bool editMode)
        {
            if (dialogService == null)
                throw new ArgumentNullException("dialogService");
            if (repository == null) 
                throw new ArgumentNullException("repository");
            if (attributeFormatParser == null)
                throw new ArgumentNullException("attributeFormatParser");
            if (autoComplete == null)
                throw new ArgumentNullException("autoComplete");
            if (attribute == null)
                throw new ArgumentNullException("attribute");

            _type = type;
            _dialogService = dialogService;
            _repository = repository;
            _attributeFormatParser = attributeFormatParser;
            _autoComplete = autoComplete;
            _editMode = editMode;

            Attribute = attribute;
            _originalValue = DValue.GetDValue(initValue);
            _value = initValue;
            IsReadOnly = isReadOnly;

            Format = _attributeFormatParser.GetAttributeFormat(Attribute.Configuration);
            if (_originalValue != null && (_originalValue.DateValue != null && Format == null))
                Format = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;

            Culture = _attributeFormatParser.GetAttributeFormatCulture(Attribute.Configuration);
        }

        private static bool IsReadOnlyReferenceBookAttribute(AttributeWrapper attribute, IAttributeFormatParser attributeFormatParser)
        {
            if (attribute.Configuration == null)
                return false;

            IReferenceBookConfiguration attributeConfiguration;
            var success = attributeFormatParser.TryParseReferenceBookConfiguration(attribute.Configuration, out attributeConfiguration);

            return success && !attributeConfiguration.IsEditable;
        }

        public bool IsNotEditableReferenceBookAttribute
        {
            get { return IsReadOnlyReferenceBookAttribute(Attribute, _attributeFormatParser); }
        }

        public IObjectsRepository Repository
        {
            get { return _repository; }
        }

        public IObjectCardAutoComplete AutoComplete
        {
            get { return _autoComplete; }
        }

        public bool IsDataChanged
        {
            get
            {
                var dValue = DValue.GetDValue(Value);
                return !_originalValue.Equals(dValue);
            }
        }

        public TypeWrapper Type { get { return _type; } }

        public AttributeWrapper Attribute { get; private set; }

        public IAttributeFormatParser AttributeFormatParser { get { return _attributeFormatParser; } }

        public bool IsReadOnly
        {
            get { return _isReadOnly; }
            set
            {
                _isReadOnly = value;
                NotifyPropertyChanged("IsReadOnly");
            }
        }

        public object Value
        {
            get { return _value; }
            set
            {
                _value = value;
                NotifyPropertyChanged("Value");
            }
        }

        public string Format
        {
            get { return _format; }
            set
            {
                _format = value;
                NotifyPropertyChanged("Format");
            }
        }

        public string Culture
        {
            get { return _culture; }
            set
            {
                _culture = value;
                NotifyPropertyChanged("Culture");
            }
        }

        public bool HasValidationError
        {
            get { return _hasValidationError; }
            set
            {
                _hasValidationError = value;
                NotifyPropertyChanged("HasValidationError");
            }
        }

        public string Error { get; set; }

        public ICommand ShowReferenceBookViewCommand
        {
            get { return new DelegateCommand(ShowReferenceBookView); }
        }

        public bool EditMode
        {
            get { return _editMode; }
        }

        [DllImport("user32.dll")]
        public static extern IntPtr GetActiveWindow();

        private void ShowReferenceBookView()
        {
            IReferenceBookConfiguration configuration;
            var success = _attributeFormatParser.TryParseReferenceBookConfiguration(Attribute.Configuration, out configuration);
            if(!success)
                return;

            if (configuration.Kind == RefBookKind.OrgUnit)
            {
                var dialogOptions = _dialogService.NewOptions()
                    .WithAllowChecking(true)
                    .WithAllowMultiSelect(true)
                    .WithOkButtonCaption("Ok")
                    .WithParentWindow(GetActiveWindow());

                var selectedPositions = _dialogService.ShowPositionSelectorDialog(dialogOptions);

                var positionsTitles = selectedPositions.Select(x => GetEffectiveAttributeTitle(x.Id));
                Value = string.Join("; ", positionsTitles);
                return;
            }

            var checkedNodes = _dialogService.ShowReferenceBookDialog(Attribute.Configuration, _type.Name + "-" + Attribute.Name,
                _dialogService.NewOptions().WithParentWindow(GetActiveWindow()).WithAllowChecking(true))
                .Select(x => new DataObjectWrapper(x, _repository)).ToList();

            if (!checkedNodes.Any())
                return;

            var titles = checkedNodes.Select(n => GetEffectiveAttributeTitle(n, configuration.StringFormat));
            Value = string.Join("; ", titles);

            if (checkedNodes.Count() == 1)
                foreach (var attribute in checkedNodes.First().Attributes)
                    AutoComplete.Fill(attribute.Key, attribute.Value);
        }

        private string GetEffectiveAttributeTitle(DataObjectWrapper obj, string stringFormat)
        {
            var formattedTitle = string.IsNullOrEmpty(stringFormat)
                ? obj.DisplayName
                : _attributeFormatParser.AttributesFormat(stringFormat, obj.Attributes.ToDictionary(x=>x.Key, x=>x.Value));
            return formattedTitle;
        }

        private string GetEffectiveAttributeTitle(int orgUnitId)
        {
            var people = _repository.GetPeople().Select(x => new PersonWrapper(x)).ToList();
            var person = people.Where(p => p.Positions.Select(m => m.Position).Contains(orgUnitId))
                .OrderBy(o => o.Positions.First(x => x.Position == orgUnitId).Order)
                .FirstOrDefault();

            return person == null ? string.Empty : person.DisplayName;
        }
    }
}
