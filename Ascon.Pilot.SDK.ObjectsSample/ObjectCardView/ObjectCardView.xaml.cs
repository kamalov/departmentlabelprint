﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ascon.Pilot.SDK.ObjectsSample.ObjectCardView
{
    /// <summary>
    /// Interaction logic for ObjectCardView.xaml
    /// </summary>
    public partial class ObjectCardView : UserControl
    {
        public ObjectCardView()
        {
            InitializeComponent();
        }
    }
}
