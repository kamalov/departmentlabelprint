﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace Ascon.Pilot.SDK.ObjectsSample.ObjectCardView
{
    class ObjectCardViewModel : PropertyChangedBase, IObjectCardAutoComplete, IDataErrorInfo
    {
        public event EventHandler IsValidInputChanged;

        private readonly DataObjectWrapper _object;
        private readonly IObjectsRepository _repository;
        private readonly IPilotDialogService _dialogService;
        private readonly IAttributeFormatParser _attributeFormatParser;
        private string _createdBy;
        private TypeWrapper _type;
        private readonly Dictionary<string, object> _serviceAttributes = new Dictionary<string, object>();
        private ObservableCollection<CardControlViewModel> _viewModelCollection;
        private bool _isValidInput;
        private bool _isReadOnly;

        internal event EventHandler<EventArgs> ValuesChanged;

        public ObjectCardViewModel(DataObjectWrapper @object, IObjectsRepository repository, IPilotDialogService dialogService, IAttributeFormatParser attributeFormatParser)
        {
            _object = @object;
            _repository = repository;
            _dialogService = dialogService;
            _attributeFormatParser = attributeFormatParser;
            _viewModelCollection = new ObservableCollection<CardControlViewModel>();

            CreateCard(_object.Type, _object);
        }

        public ObservableCollection<CardControlViewModel> ViewModelCollection
        {
            get { return _viewModelCollection; }
            set { _viewModelCollection = value; }
        }

        public string ObjectTypeTitle
        {
            get { return _object.Type.Title; }
        }

        public string CreatedBy
        {
            get { return _createdBy; }
            set
            {
                _createdBy = value;
                NotifyPropertyChanged("CreatedBy");
            }
        }

        public Dictionary<string, DValue> Values
        {
            get
            {
                var values = new Dictionary<string, DValue>();
                foreach (var cardControlViewModel in _viewModelCollection.Where(x => x.Value != null))
                {
                    values[cardControlViewModel.Attribute.Name] = DValue.GetDValue(cardControlViewModel.Value);
                }
                foreach (var serviceAttribute in _serviceAttributes)
                {
                    values[serviceAttribute.Key] = DValue.GetDValue(serviceAttribute.Value);
                }
                return values;
            }
        }

        public bool IsValidInput
        {
            get { return _isValidInput; }
            set
            {
                _isValidInput = value;
                RaiseIsValidInputChanged();
                NotifyPropertyChanged("IsValidInput");
            }
        }

        public bool IsReadOnly
        {
            get { return _isReadOnly; }
            set
            {
                _isReadOnly = value;
                foreach (var cardControlViewModel in _viewModelCollection)
                    cardControlViewModel.IsReadOnly = value;

                NotifyPropertyChanged("IsReadOnly");
            }
        }

        private bool _isCardUpdated;
        public bool IsCardUpdated
        {
            get { return _isCardUpdated; }
            set
            {
                _isCardUpdated = value;
                InvalidateIsValidInput();
            }
        }

        public IAttributeFormatParser AttributeFormatParser { get { return _attributeFormatParser; } }

        public bool IsDataChanged
        {
            get { return _viewModelCollection.Any(x => x.IsDataChanged); }
        }

        public void Save()
        {
            MessageBox.Show("Saving routine here.");
        }

        /// <summary>
        /// Создать карточку.
        /// </summary>
        /// <param name="type">Тип объекта</param>
        /// <param name="editedObject">Объект для редактирования. В случае создания нового объекта - null</param>
        public void CreateCard(TypeWrapper type, DataObjectWrapper editedObject = null)
        {
            _type = type;

            CreatedBy = null;
            _viewModelCollection.Clear();

            if (type != null && type.Attributes.Count > 0)
            {
                var attributes = type.Attributes.OrderBy(u => u.DisplaySortOrder).Where(a => a.IsService == false).ToArray();
                for (int i = 0; i < attributes.Length; i++)
                {
                    var attribute = attributes[i];
                    var initValue = editedObject != null
                        ? GetAttrValue(editedObject, attribute.Name)
                        : GetNewAttrValue(attribute);
                    var cardControlViewModel = new CardControlViewModel(attribute, type, initValue, IsReadOnly, _dialogService, _repository, this, _attributeFormatParser, editedObject != null);

                    cardControlViewModel.PropertyChanged += (o, e) => InvalidateIsValidInput();
                    _viewModelCollection.Add(cardControlViewModel);
                }
            }

            if (editedObject == null)
                return;

            var creatorName = editedObject.Creator.DisplayName;
            if (string.IsNullOrEmpty(creatorName))
                creatorName = editedObject.Creator.Login;

            CreatedBy = string.Format("Created by {0} {1}", creatorName, editedObject.Created.ToLocalTime().ToString("g"));
            InvalidateIsValidInput();
        }

        private void InvalidateIsValidInput()
        {
            var isDirty = _viewModelCollection.Any(viewModel => viewModel.Value != null);
            var hasValidationError = _viewModelCollection.Any(viewModel => viewModel.HasValidationError);
            var obligatoryFieldsAreNotEmpty = _viewModelCollection.All(viewModel => !viewModel.Attribute.IsObligatory || (viewModel.Value != null && !string.IsNullOrWhiteSpace(viewModel.Value.ToString())));
            IsValidInput = isDirty && !hasValidationError && obligatoryFieldsAreNotEmpty;
            NotifyPropertyChanged("Values");
        }

        private void RaiseIsValidInputChanged()
        {
            if (IsValidInputChanged != null)
                IsValidInputChanged(this, EventArgs.Empty);
        }

        public void Fill(string attributeName, object value)
        {
            var viewModel = _viewModelCollection.FirstOrDefault(x => x.Attribute.Name == attributeName);
            if (viewModel != null)
            {
                // Отображаемый в карточке атрибут (кроме справочников)
                if (string.IsNullOrEmpty(viewModel.Attribute.Configuration))
                    viewModel.Value = value;
            }
            else
            {
                // Неотображаемый в карточке атрибут
                if (_type.Attributes.Any(x => x.Name == attributeName))
                    _serviceAttributes[attributeName] = value;
            }
            OnValuesChanged();
        }

        protected virtual void OnValuesChanged()
        {
            var handler = ValuesChanged;
            if (handler != null)
                handler(this, EventArgs.Empty);
            NotifyPropertyChanged("Values");
        }

        private static object GetAttrValue(DataObjectWrapper localObject, string attrName)
        {
            object value;
            if (!localObject.Attributes.TryGetValue(attrName, out value))
                return null;
            return value;
        }

        private static object GetNewAttrValue(AttributeWrapper attribute)
        {
            if (attribute.Type == AttributeType.DateTime)
                return DateTime.Now;
            return null;
        }

        #region validation

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                CardControlViewModel model = _viewModelCollection.First(c => c.Attribute.Name == columnName);
                if (!model.HasValidationError)
                    return model.Error;

                return String.Empty;
            }
        }
        #endregion
    }
}
