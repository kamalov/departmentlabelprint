﻿using System;
using System.Windows;
using System.Windows.Controls;
using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.ObjectsSample.ObjectCardView;

namespace Ascon.Pilot.Client.ObjectCard
{
    public class CardEditorTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var viewModel = (CardControlViewModel)item;
            if (viewModel == null)
                return null;

            switch (viewModel.Attribute.Type)
            {
                case AttributeType.String:
                    if (!string.IsNullOrEmpty(viewModel.Attribute.Configuration) && !viewModel.IsReadOnly)
                    {
                        IReferenceBookConfiguration configuration;
                        var success = viewModel.AttributeFormatParser.TryParseReferenceBookConfiguration(viewModel.Attribute.Configuration, out configuration);

                        if (!success)
                            return CardEditorTemplates.Instance.StringEditTemplate;

                        switch (configuration.EditorType)
                        {
                            case RefBookEditorType.ComboBox:
                                {
                                    switch (configuration.Kind)
                                    {
                                        case RefBookKind.Enum:
                                            return CardEditorTemplates.Instance.ReferenceBookComboBoxEnumEditTemplate;
                                        case RefBookKind.Object:
                                            return CardEditorTemplates.Instance.ReferenceBookComboBoxEditTemplate;
                                        case RefBookKind.OrgUnit:
                                            return CardEditorTemplates.Instance.ReferenceBookComboBoxOrgUnitEditTemplate;
                                        default:
                                            throw new ArgumentOutOfRangeException();
                                    }
                                }
                            case RefBookEditorType.Dialog:
                                return CardEditorTemplates.Instance.ReferenceBookDialogEditTemplate;
                            default:
                                throw new NotSupportedException("ReferenceBookAttributeEditorType." + configuration.EditorType);
                        }
                    }
                    return CardEditorTemplates.Instance.StringEditTemplate;
                case AttributeType.Integer:
                    return CardEditorTemplates.Instance.LongEditTemplate;
                case AttributeType.Double:
                    return CardEditorTemplates.Instance.DoubleEditTemplate;
                case AttributeType.DateTime:
                    return CardEditorTemplates.Instance.DateEditTemplate;
                case AttributeType.Decimal:
                    return CardEditorTemplates.Instance.DecimalEditTemplate;
                case AttributeType.Numerator:
                    return viewModel.EditMode
                        ? CardEditorTemplates.Instance.NumeratorStringEditTemplate
                        : CardEditorTemplates.Instance.NumeratorEditTemplate;
                default:
                    throw new NotSupportedException();
            }
        }
    }

    partial class CardEditorTemplates
    {
        internal static readonly CardEditorTemplates Instance = new CardEditorTemplates();

        public DataTemplate DateEditTemplate
        {
            get { return (DataTemplate)this["DateEditTemplate"]; }
        }

        public DataTemplate LongEditTemplate
        {
            get { return (DataTemplate)this["LongEditTemplate"]; }
        }

        public DataTemplate DoubleEditTemplate
        {
            get { return (DataTemplate)this["DoubleEditTemplate"]; }
        }

        public DataTemplate DecimalEditTemplate
        {
            get { return (DataTemplate)this["DecimalEditTemplate"]; }
        }

        public DataTemplate StringEditTemplate
        {
            get { return (DataTemplate)this["StringEditTemplate"]; }
        }

        public DataTemplate NumeratorEditTemplate
        {
            get { return (DataTemplate)this["NumeratorEditTemplate"]; }
        }

        public DataTemplate NumeratorStringEditTemplate
        {
            get { return (DataTemplate)this["NumeratorStringEditTemplate"]; }
        }

        public DataTemplate ReferenceBookDialogEditTemplate
        {
            get { return (DataTemplate)this["ReferenceBookDialogEditTemplate"]; }
        }

        public DataTemplate ReferenceBookComboBoxEditTemplate
        {
            get { return (DataTemplate)this["ReferenceBookComboBoxEditTemplate"]; }
        }

        public DataTemplate ReferenceBookComboBoxEnumEditTemplate
        {
            get { return (DataTemplate)this["ReferenceBookComboBoxEnumEditTemplate"]; }
        }

        public DataTemplate ReferenceBookComboBoxOrgUnitEditTemplate
        {
            get { return (DataTemplate)this["ReferenceBookComboBoxOrgUnitEditTemplate"]; }
        }

        private CardEditorTemplates()
        {
            InitializeComponent();
        }
    }
}
