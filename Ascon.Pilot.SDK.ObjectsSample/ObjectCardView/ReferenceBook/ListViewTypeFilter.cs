﻿using System;

namespace Ascon.Pilot.SDK.ObjectsSample.ObjectCardView.ReferenceBook
{
    public class ListViewTypeFilter 
    {
        private readonly Func<IObjectsRepository> _getRepository;

        public ListViewTypeFilter(Func<IObjectsRepository> getRepository)
        {
            _getRepository = getRepository;
        }

        public bool Matches(int typeId)
        {
            var type = _getRepository().GetType(typeId);
            return Matches(new TypeWrapper(type));
        }

        private bool Matches(TypeWrapper type)
        {
            if (type.Name == SystemTypeNames.SMART_FOLDER)
                return true;
            if (type.Name == SystemTypeNames.SHORTCUT)
                return true;
            return !type.IsService && type.Kind == TypeKind.User;
        }
    }
}
