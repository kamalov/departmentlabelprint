﻿using System.Windows;
using Ascon.Pilot.SDK.ObjectsSample.NodeViewModel;

namespace Ascon.Pilot.SDK.ObjectsSample
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView 
    {
        public MainView()
        {
            InitializeComponent();
            Loaded += OnLoaded;
        }

        void OnLoaded(object sender, RoutedEventArgs e)
        {
            UpdateIsSharingSettingsButtonEnabled();
        }

        private void OnSharingSettingsButtonClick(object sender, RoutedEventArgs e)
        {
            var viewModel = (MainViewModel) DataContext;
            var selectedObject = TreeView.SelectedValue as ElementNodeViewModel;
            if(selectedObject != null)
                viewModel.ShowSharingSettingsDialog(selectedObject.Id);
        }

        private void OnSelectedObjectChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            UpdateIsSharingSettingsButtonEnabled();
        }

        private void UpdateIsSharingSettingsButtonEnabled()
        {
            var selectedObject = TreeView.SelectedValue as ElementNodeViewModel;
            SharingSettingsButton.IsEnabled = selectedObject != null;
        }

        private void OnDocumentSelectorButton(object sender, RoutedEventArgs e)
        {
            var viewModel = (MainViewModel)DataContext;
            viewModel.ShowDocumentSelector();
        }

        private void OnPositionSelectorButton(object sender, RoutedEventArgs e)
        {
            var viewModel = (MainViewModel)DataContext;
            viewModel.ShowPositionSelector();
        }

        private void OnTaskSelectorButton(object sender, RoutedEventArgs e)
        {
            var viewModel = (MainViewModel)DataContext;
            viewModel.ShowTasksSelector();
        }
    }
}
