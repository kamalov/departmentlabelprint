﻿using System;

namespace Ascon.Pilot.SDK.ObjectsSample.NodeViewModel
{
    class TypeNodeViewModel : PropertyChangedBase, IObserver<IType>
    {
        private TypeWrapper _type;
        private string _title;

        public TypeNodeViewModel(IType type, IObjectsRepository repository)
        {
            _type = new TypeWrapper(type);
            _title = _type.Title;
            repository.SubscribeTypes().Subscribe(this);
        }

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                NotifyPropertyChanged("Title");
            }
        }

        public void OnNext(IType value)
        {
            if (value.Id != _type.Id) 
                return;
            
            _type = new TypeWrapper(value);
            Title = _type.Title;
        }

        public void OnError(Exception error)
        {
        }

        public void OnCompleted()
        {
        }
    }
}
