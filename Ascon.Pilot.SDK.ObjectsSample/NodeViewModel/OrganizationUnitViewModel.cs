﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace Ascon.Pilot.SDK.ObjectsSample.NodeViewModel
{
    class OrganizationUnitViewModel : PropertyChangedBase, IOrganizationUnitViewModel, IObserver<IOrganisationUnit>
    {
        private OrganisationUnitWrapper _organisation;
        private readonly IObjectsRepository _repository;
        private string _title;
        private readonly ObservableCollection<IOrganizationUnitViewModel> _children;
        private readonly ReadOnlyCollection<int> _vicePersons;

        public OrganizationUnitViewModel(IOrganisationUnit organisation, IObjectsRepository repository)
        {
            _organisation = new OrganisationUnitWrapper(organisation);
            _title = _organisation.Title + " - " + organisation.Kind();

            _children = new ObservableCollection<IOrganizationUnitViewModel>();

            _repository = repository;
            _repository.SubscribeOrganisationUnits().Subscribe(this);

            _vicePersons = organisation.VicePersons();
        }

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                NotifyPropertyChanged("Title");
            }
        }

        public ObservableCollection<IOrganizationUnitViewModel> Children
        {
            get
            {
                LoadChildren();
                return _children;
            }    
        }

        public void OnNext(IOrganisationUnit value)
        {
            if (value.Id != _organisation.Id) 
                return;
            
            _organisation = new OrganisationUnitWrapper(value);
            Title = _organisation.Title;

            //TODO: check children changes
        }

        public void OnError(Exception error)
        {
        }

        public void OnCompleted()
        {
        }

        private void LoadChildren()
        {
            _children.Clear();

            foreach (var childId in _organisation.Children)
            {
                var child = _repository.GetOrganisationUnit(childId);
                if (!child.IsDeleted)
                    _children.Add(new OrganizationUnitViewModel(child, _repository));
            }

            var people = _repository.GetPeople().ToList();
            foreach (var person in people)
            {
                var exists = (person.MainPosition != null && person.MainPosition.Position == _organisation.Id);
                if (exists)
                    _children.Add(new PersonNodeViewModel(person, _repository, false));
            }

            foreach (var viceId in _vicePersons)
            {
                var person = _repository.GetPerson(viceId);
                if (!person.IsDeleted)
                    _children.Add(new PersonNodeViewModel(person, _repository, true));
            }
        }
    }
}
