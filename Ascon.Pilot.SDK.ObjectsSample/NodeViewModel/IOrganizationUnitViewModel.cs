﻿using System.Collections.ObjectModel;

namespace Ascon.Pilot.SDK.ObjectsSample.NodeViewModel
{
    interface IOrganizationUnitViewModel
    {
        string Title { get; }
        ObservableCollection<IOrganizationUnitViewModel> Children { get; }
    }
}
