﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ascon.Pilot.SDK.ObjectsSample.ObjectCardView;
using Ascon.Pilot.Theme.Controls;
using Microsoft.Practices.Prism.Commands;

namespace Ascon.Pilot.SDK.ObjectsSample.NodeViewModel
{
    class RootNodeViewModel : ElementNodeViewModel
    {
        public RootNodeViewModel(IDataObject source, IObjectsRepository repository,
            ITabServiceProvider tabServiceProvider, IAttributeFormatParser attributeFormatParser,
            IPilotDialogService dialogService, IObjectModifier modifier) :
            base(source, repository, tabServiceProvider, attributeFormatParser, dialogService, modifier)
        {
            ShowChildren = true;
        }

        public override bool IsRoot => true;

        public override string DisplayName => "Root";
    }

    class ElementNodeViewModel : PropertyChangedBase, ITreeNode, INotifySortPropertyChanged, IObserver<IDataObject>
    {
        private readonly SortedTreeNodesCollection<ElementNodeViewModel> _children;

        private string _displayName;
        private DataObjectWrapper _source;
        private readonly IObjectsRepository _repository;
        private readonly ITabServiceProvider _tabServiceProvider;
        private readonly IObjectModifier _modifier;
        private readonly IPilotDialogService _dialogService;
        private readonly IAttributeFormatParser _attributeFormatParser;
        private readonly DelegateCommand _navigateToElementCommand;
        private readonly DelegateCommand _navigateToSourceFileCommand;
        private readonly DelegateCommand _showPropertiesCommand;
        private readonly DelegateCommand _deleteCommand;
        private bool _isInactive;
        private bool _isLoading;
        private bool _isVisible = true;
        private bool _showChildren;

        public ElementNodeViewModel(IDataObject source, IObjectsRepository repository, ITabServiceProvider tabServiceProvider, IAttributeFormatParser attributeFormatParser, IPilotDialogService dialogService, IObjectModifier modifier)
        {
            if (dialogService == null)
                throw new ArgumentNullException("dialogService");

            _children = new SortedTreeNodesCollection<ElementNodeViewModel>(this, new NodesComparer());
            _source = new DataObjectWrapper(source, repository);
            _displayName = _source.DisplayName;
            _repository = repository;
            _tabServiceProvider = tabServiceProvider;
            _modifier = modifier;
            _dialogService = dialogService;
            _attributeFormatParser = attributeFormatParser;
            Id = source.Id;
            SourceFileId = source.RelatedSourceFiles.FirstOrDefault();

            _navigateToElementCommand = new DelegateCommand(NavigateToElement);
            _navigateToSourceFileCommand = new DelegateCommand(NavigateToSourceFile, CanNavigateToSourceFile);
            _showPropertiesCommand = new DelegateCommand(ShowProperties);
            _deleteCommand = new DelegateCommand(DeleteToRecycleBin);
        }

        public virtual bool IsRoot => false;
        public ITreeNode Parent { get; set; }
        public virtual ITreeNodesCollection<ITreeNode> Children => _children;
        public bool HasChildren => _source.ListViewChildren.Any();
        public bool ShowExpander => true;

        public bool ShowChildren
        {
            get { return _showChildren; }
            set
            {
                _showChildren = value;
                OnShowChildrenChanged();
                NotifyPropertyChanged("ShowChildren");
            }
        }

        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                _isVisible = true;
                NotifyPropertyChanged("IsVisible");
            }
        }

        public bool CanBeChecked => false;

        public bool IsChecked { get; set; }

        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                NotifyPropertyChanged("IsLoading");
            }
        }

        protected virtual void OnShowChildrenChanged()
        {
            if (ShowChildren)
            {
                ReloadChildren();
            }
            else
            {
                IsLoading = false;
            }
        }

        public bool IsHighlighted { get; set; }
        public event EventHandler SortPropertyChanged;

        public Guid Id { get; private set; }

        public Guid SourceFileId { get; private set; }

        public DelegateCommand NavigateToElementCommand
        {
            get { return _navigateToElementCommand; }
        }

        public DelegateCommand NavigateToSourceFileCommand
        {
            get { return _navigateToSourceFileCommand; }
        }

        public DelegateCommand ShowPropertiesCommand
        {
            get { return _showPropertiesCommand; }
        }

        public DelegateCommand DeleteCommand
        {
            get { return _deleteCommand; }
        }

        public virtual string DisplayName
        {
            get { return _displayName; }
            set
            {
                _displayName = value;
                NotifyPropertyChanged("DisplayName");
            }
        }

        public bool IsInactive
        {
            get { return true; }
            set
            {
                if (_isInactive == value)
                    return;

                _isInactive = value;
                NotifyPropertyChanged("IsInactive");
            }
        }

        public void OnNext(IDataObject value)
        {
            if (!_source.ListViewChildren.Contains(value.Id))
                return;

            if (IsLoading)
                _children.Clear();

            var node = _children.FirstOrDefault(n => n.Id == value.Id);

            if (node != null)
            {
                node.Update(value);
                return;
            }

            _children.Add(new ElementNodeViewModel(value, _repository, _tabServiceProvider, _attributeFormatParser, _dialogService, _modifier));
        }

        public void OnError(Exception error)
        {

        }

        public void OnCompleted()
        {

        }

        public void Update(IDataObject newSource)
        {
            _source = new DataObjectWrapper(newSource, _repository);
            DisplayName = _source.DisplayName;
            ReloadChildren();
        }

        private void ReloadChildren()
        {
            RemoveDeletedNodes();
            LoadNewNodes();
        }

        private void RemoveDeletedNodes()
        {
            if (IsLoading)
                return;

            foreach (var childNode in _children.ToList())
            {
                if (!_source.ListViewChildren.Contains(childNode.Id))
                    _children.Remove(childNode);
            }
        }

        private void LoadNewNodes()
        {
            if (IsLoading)
                return;

            IsLoading = true;

            var childIdsToLoad = new List<Guid>();
            foreach (var childId in _source.ListViewChildren)
            {
                if (_children.FirstOrDefault(x => x.Id == childId) == null)
                    childIdsToLoad.Add(childId);
            }

            _repository.SubscribeObjects(childIdsToLoad).Subscribe(this);

            IsLoading = false;
        }

        private void NavigateToElement()
        {
            _tabServiceProvider.ShowElement(Id);
        }

        private void NavigateToSourceFile()
        {
            _tabServiceProvider.ShowElement(SourceFileId);
        }

        private bool CanNavigateToSourceFile()
        {
            return SourceFileId != Guid.Empty;
        }

        private void ShowProperties()
        {
            var objectCardViewModel = new ObjectCardViewModel(_source, _repository, _dialogService, _attributeFormatParser);
            var dialogView = new ObjectPropertiesView();
            dialogView.DataContext = objectCardViewModel;

            var propertiesWindow = new DialogWindow()
            {
                Title = "Edit element",
                Content = dialogView,
                Width = 400,
                Height = 450
            };

            propertiesWindow.ShowDialog();
        }

        private void DeleteToRecycleBin()
        {
            _modifier.DeleteById(Id);
            _modifier.Apply();
        }
    }

    class NodesComparer : IComparer<ElementNodeViewModel>
    {
        public int Compare(ElementNodeViewModel x, ElementNodeViewModel y)
        {
            return String.Compare(x?.DisplayName ?? string.Empty, y?.DisplayName ?? string.Empty, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
