﻿using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace Ascon.Pilot.SDK.ObjectsSample.NodeViewModel
{
    class PersonNodeViewModel : PropertyChangedBase, IOrganizationUnitViewModel, IObserver<IPerson>
    {
        private PersonWrapper _person;
        private readonly IObjectsRepository _repository;
        private bool _isVice;

        public PersonNodeViewModel(IPerson person, IObjectsRepository repository, bool isVice)
        {
            _person = new PersonWrapper(person);
            _isVice = isVice;
            _repository = repository;
            Children = new ObservableCollection<IOrganizationUnitViewModel>();
            repository.SubscribePeople().Subscribe(this);
        }

        public string Title
        {
            get {
                string title = _person.DisplayName;

                title = _isVice ? "зам. " + title : title;
                title = IsInactive ? title + " (недоступен)" : title;

                return title;
            }
        }

        public bool IsInactive
        {
            get { return _person.IsInactive; }
        }


        public ObservableCollection<IOrganizationUnitViewModel> Children { get; private set; }

        public void OnNext(IPerson value)
        {
            if (value.Id != _person.Id) 
                return;

            _person = new PersonWrapper(value);

            NotifyPropertyChanged(string.Empty);
        }

        public void OnError(Exception error)
        {
        }

        public void OnCompleted()
        {
        }
    }
}
