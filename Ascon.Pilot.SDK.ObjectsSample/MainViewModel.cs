﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Ascon.Pilot.SDK.ObjectsSample.NodeViewModel;
using Ascon.Pilot.SDK.CreateObjectSample;

namespace Ascon.Pilot.SDK.ObjectsSample
{
    class MainViewModel : PropertyChangedBase, IObserver<IDataObject>, IObserver<bool>
    {
        private readonly IObjectsRepository _repository;
        private readonly IPilotDialogService _pilotDialogService;
        private readonly ITabServiceProvider _tabServiceProvider;
        private readonly IAttributeFormatParser _attributeFormatParser;
        private readonly IObjectModifier _modifier;
        private RootNodeViewModel _objectsRoot;
        private readonly ObservableCollection<IOrganizationUnitViewModel> _orgUnits;
        private readonly ObservableCollection<TypeNodeViewModel> _types;
        private DataObjectWrapper _baseElement;
        private bool _isConnected;
        
        public MainViewModel(
            IObjectsRepository repository, 
            IPilotDialogService pilotDialogService, 
            ITabServiceProvider tabServiceProvider, 
            IAttributeFormatParser attributeFormatParser, 
            IObjectModifier modifier,
            IConnectionStateProvider connectionStateProvider)
        {
            _orgUnits = new ObservableCollection<IOrganizationUnitViewModel>();
            _types = new ObservableCollection<TypeNodeViewModel>();
            _repository = repository;
            _pilotDialogService = pilotDialogService;
            _tabServiceProvider = tabServiceProvider;
            _attributeFormatParser = attributeFormatParser;
            _modifier = modifier;

            var loader = new ObjectLoader(_repository);
            Task.Factory.StartNew(async () =>
            {
                var obj = await loader.Load(SystemObjectIds.RootObjectId);
                _baseElement = new DataObjectWrapper(obj, repository);
                _repository.SubscribeObjects(new[] { _baseElement.Id }).Subscribe(this);
            });

            LoadOrganizationalStructure();
            LoadTypes();

            IsConnected = connectionStateProvider.IsOnline();
            connectionStateProvider.Subscribe().Subscribe(this);
        }

        public RootNodeViewModel ObjectsRoot
        {
            get { return _objectsRoot; }
            set
            {
                _objectsRoot = value;
                NotifyPropertyChanged("ObjectsRoot");
            }
        }

        public ObservableCollection<IOrganizationUnitViewModel> OrgUnits
        {
            get { return _orgUnits; }
        }

        public ObservableCollection<TypeNodeViewModel> Types
        {
            get { return _types; }
        }

        public string CurrentPerson
        {
            get { return _repository.GetCurrentPerson().ActualName; }
        }

        public bool IsConnected
        {
            get { return _isConnected; }
            set
            {
                _isConnected = value;
                NotifyPropertyChanged("IsConnected");
            }
        }

        public void OnNext(bool isConnected)
        {
            IsConnected = isConnected;
        }

        public void ShowSharingSettingsDialog(Guid objectId)
        {
            _pilotDialogService.ShowSharingSettingsDialog(new[] { objectId });
        }

        public void ShowDocumentSelector()
        {
            var selectedDocuments = _pilotDialogService.ShowDocumentsSelectorDialog();
            var message = selectedDocuments.Any()
                ? "Selected objects list: " + Environment.NewLine +  string.Join(Environment.NewLine, selectedDocuments.Select(x => x.DisplayName))
                : "No objects have been selected";

            MessageBox.Show(message);
        }

        public void ShowPositionSelector()
        {
            var selectedPositions = _pilotDialogService.ShowOrganisationUnitSelectorDialog();
            var message = selectedPositions.Any()
                ? "Selected positions: " + Environment.NewLine + string.Join(Environment.NewLine, selectedPositions.Select(x => x.Title))
                : "No positions have been selected";

            MessageBox.Show(message);
        }

        public void ShowTasksSelector()
        {
            var selectedTaskObjects = _pilotDialogService.ShowTasksSelectorDialog2().ToList();
            var message = selectedTaskObjects.Any()
                ? "Selected tasks list: " + Environment.NewLine + string.Join(Environment.NewLine, selectedTaskObjects.Select(x => x.DisplayName))
                : "No objects have been selected";

            MessageBox.Show(message);
        }

        public void OnNext(IDataObject value)
        {
            if (value.Id == _baseElement.Id)
            {
                _baseElement = new DataObjectWrapper(value, _repository);
                ObjectsRoot = new RootNodeViewModel(value, _repository, _tabServiceProvider, _attributeFormatParser, _pilotDialogService, _modifier);
            }
        }

        public void OnError(Exception error)
        {
        }

        public void OnCompleted()
        {
        }

        private void LoadOrganizationalStructure()
        {
            var root = _repository.GetOrganisationUnits().FirstOrDefault();
            if (root == null)
                return;

            foreach (var id in root.Children)
            {
                var child = _repository.GetOrganisationUnit(id);
                if (!child.IsDeleted)
                    _orgUnits.Add(new OrganizationUnitViewModel(child, _repository));
            }
        }

        private void LoadTypes()
        {
            var types = _repository.GetTypes();
            foreach (var type in types)
            {
                if (type.Kind == TypeKind.User && !type.IsDeleted)
                    _types.Add(new TypeNodeViewModel(type, _repository));
            }
        }
    }
}
