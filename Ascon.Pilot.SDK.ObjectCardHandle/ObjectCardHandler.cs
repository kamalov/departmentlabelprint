﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using Ascon.Pilot.SDK.ObjectCard;

namespace Ascon.Pilot.SDK.ObjectCardHandle
{
    [Export(typeof(IObjectCardHandler))]
    public class ObjectCardHandler : IObjectCardHandler
    {
        public bool Handle(IAttributeModifier modifier, ObjectCardContext context)
        {
            var isObjectModification = context.EditiedObject != null;
            if (isObjectModification || context.IsReadOnly)
                return false;

            var parent = context.Parent;
            var sourceAttr = context.DisplayAttributes.FirstOrDefault(a => a.Type == AttributeType.String);
            if (sourceAttr == null)
                return false;

            var sourceValue = parent.Attributes.FirstOrDefault(a => a.Key == sourceAttr.Name);
            if (sourceValue.Value == null)
                return false;

            var targetAttr = context.DisplayAttributes.FirstOrDefault(a => a.Type == AttributeType.String);
            if (targetAttr == null)
                return false;

            var valueToSet = string.Format("Parent is {0}; IsDocument:{1}; Can be mount:{2}", sourceValue.Value, context.Parent.Type.HasFiles, context.Parent.Type.IsMountable);

            modifier.SetValue(targetAttr.Name, valueToSet);
            return true;
        }

        public bool OnValueChanged(IAttribute sender, AttributeValueChangedEventArgs args, IAttributeModifier modifier)
        {
            var currentAttributeValues = string.Empty;
            foreach (var displayAttribute in args.Context.DisplayAttributes)
            {
                currentAttributeValues += displayAttribute.Name == sender.Name 
                    ? args.NewValue
                    : displayAttribute.Name + ": " + args.Context.AttributesValues[displayAttribute.Name] + Environment.NewLine;
            }

            if (args.Context.Type.Name == "Document" && sender.Name == "Sheet_number")
            {
                var newNameAttrValue = "Sheet no " + args.NewValue + "; " + (args.Context.EditiedObject == null ? " New object " : " Existed object");
                modifier.SetValue("Name", newNameAttrValue);
                return true;
            }

            return false;
        }
    }
}
