﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Ascon.Pilot.SDK.PrinterSample
{
    class SettingsViewModel : INotifyPropertyChanged, IObserver<KeyValuePair<string, string>>, IObserver<IDataObject>
    {
        private readonly IObjectsRepository _repository;
        private readonly ObservableCollection<SettingsFeatureNode> _settings = new ObservableCollection<SettingsFeatureNode>();
        private readonly SettingsFeatureNode _folderSettingNode;


        public event PropertyChangedEventHandler PropertyChanged;

        public SettingsViewModel(IPersonalSettings personalSettings, IObjectsRepository repository)
        {
            _repository = repository;
           _folderSettingNode = new SettingsFeatureNode("Папка автокопирования");
           _settings.Add(_folderSettingNode);
           personalSettings.SubscribeSetting(SettingsFeatureKeys.FolderSettingKey).Subscribe(this);
            
        }

        public ObservableCollection<SettingsFeatureNode> Settings
        {
            get { return _settings; }
        }

        public void OnNext(KeyValuePair<string, string> value)
        {
            if (value.Value == null)
                return;
            

            if (value.Key == SettingsFeatureKeys.FolderSettingKey)
            {
                _folderSettingNode.Settings.Add(value.Value);
            }
        }

        public void OnNext(IDataObject value)
        {
           
        }

        public void OnError(Exception error)
        {
            
        }

        public void OnCompleted()
        {
            
        }
    }
}
