﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Ascon.Pilot.SDK.PrinterSample
{
    class ViewerProcess
    {

        private Process myViewerProcess;
        private TaskCompletionSource<bool> eventHandled;
        private string _filename;

        public ViewerProcess(string filename)
        {
            _filename = filename;
            
        }

        public async Task ViewDocTask(string fileName)
        {
            eventHandled = new TaskCompletionSource<bool>();
            using (myViewerProcess = new Process())

                try
                {
                    // Запустить процесс вьювера и выдать событие, когда вьювер закрыт
                    myViewerProcess.StartInfo.FileName = fileName;
                    //myViewerProcess.StartInfo.CreateNoWindow = true;
                    myViewerProcess.EnableRaisingEvents = true;
                    myViewerProcess.Exited += MyViewerProcess_Exited;
                    myViewerProcess.Start();
                    
                    
                }
                catch (Exception e)
                {
                    MessageBox.Show( e.ToString());

                }
            // Wait for Exited event
            await Task.WhenAny(eventHandled.Task);
        }

        // Handle Exited event
        private void MyViewerProcess_Exited(object sender, EventArgs e)
        {
            MessageBox.Show("Событие о выходе получено");
            File.Delete(_filename);
            eventHandled.TrySetResult(true);
        }
    }
}
