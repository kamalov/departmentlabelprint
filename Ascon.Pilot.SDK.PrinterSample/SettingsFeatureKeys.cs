﻿namespace Ascon.Pilot.SDK.PrinterSample
{
    static class SettingsFeatureKeys
    {
        public static string SampleFeatureKey
        {
            get
            {
                return "SampleSetting-5525E0CC-F8D0-40BF-A027-5E5C1832F387"; // bak SampleSetting-5525E0CC-F8D0-40BF-A027-5E5C1832F387
            }
        }

        public static string Sample2FeatureKey
        {
            get
            {
                return "SampleSetting2-CCB5BEEE-051D-479F-91B3-8611681B8D0C";
            }
        }

        public static string FolderSettingKey
        {
            get
            {
                return "FolderSettingKey-CCB5BEED-051D-469A-B197-5E4D1832F381";
            }
        }
    }
}
