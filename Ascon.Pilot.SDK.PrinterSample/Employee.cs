﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ascon.Pilot.SDK.PrinterSample
{
    class Employee
    {
        private IObjectsRepository _repository;
        private int _personID;
        private string _personDepartment;
        private string _positionName;
        private string _personActualname; // По умолчанию это поле выводит информацию из поля DisplayName. Если поле DisplayName не заполнено, то данное поле выводит информацию из поля Login



        public Employee(int personID, IObjectsRepository repository)
        {
            _personID = personID;
            _repository = repository;
            _personActualname = _repository.GetCurrentPerson().ActualName;

            
            var positionID = _repository.GetCurrentPerson().MainPosition.Position; // получаем идентификатор основной должности
            _positionName = _repository.GetOrganisationUnit(positionID).Title; // получаем наименование основной должности
             var allPositions = _repository.GetCurrentPerson().AllOrgUnits(); // получаем все должности

            foreach (var p in allPositions) //находим наименование отдела, где пользователь занимает главную должность
            {
               // TODO: переписать в виде лямбда-выражения
                var orgUnit = _repository.GetOrganisationUnit(p);
                var orgUnitChildren = orgUnit.Children;
                if (orgUnitChildren != null)
                {
                    foreach (var ch in orgUnitChildren)
                    {
                        if (ch == positionID)
                        {
                            _personDepartment = orgUnit.Title ;
                        }

                    }
                    
                }
            }

        }
        public string Depatrment
        {
            // возвращает наименование отдела, где пользователь занимает главную должность
            get { return _personDepartment; }
        }
        public string PositionName
        {
            // возвращает наименование основной должности
            get { return _positionName; }
        }
        public string ActualName
        {
            // возвращает данные из  поля DisplayName. Если оно пустое, возвращает Login
            get { return _personActualname; }
        }



}

       
    }

