﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Ascon.Pilot.SDK.CreateObjectSample;
using Ascon.Pilot.SDK.Menu;
using Ascon.Pilot.Theme.ColorScheme;
using System.Xml.Serialization;
using static System.Net.Mime.MediaTypeNames;

namespace Ascon.Pilot.SDK.PrinterSample
{
    [Export(typeof(IPrintHandler))]
    [Export(typeof(IObjectContextMenu))]
    [Export(typeof(IMainMenu))]


    public class PrintHandler : IPrintHandler, IObjectContextMenu, IFileSaver, IClientInfo, IMainMenu, IMouseLeftClickListener, IObserver<KeyValuePair<string, string>>
    {
        private readonly IObjectsRepository _repository;
        private readonly IFileSaver _filesaver;
        private readonly IClientInfo _clientinfo;
        private readonly IPersonalSettings _personalSettings;
        private readonly ISettingValueProvider _valueProvider;
        private readonly IObjectModifier _modifier;

        public bool departmentcopyFlag;
        public const string UserTempDir = "departmentPrint";

        public const string Xaml = "<TextBlock Foreground=\"Red\" FontSize=\"20\">НАПЕЧАТАНО</TextBlock>";
        public const string XamlStart = "<TextBlock Foreground=\"Black\" FontSize=\"16\">";
        public const string XamlClose = "</TextBlock>";
        const string DEPARTMENT_FOLDER_TYPE = "departmentFolder";
        private const string MAKE_COPY = "MakeMyCopy";

        private IDataObject _xpsDoc;
        private List<IDataObject> _selectedObjects;
        private IDictionary<int, IType> _types;
        private readonly List<int> openTypesIdcInts = new List<int>();
        private string _dirtyFolderGUIDStr;
        private Guid dirtyFolderGuid;

        private IPerson currentUser;

        public bool autoimportCopyFlag
        {
            get { return autoimportCopyFlag; }
            set { }
        }
        public Uri ConnectionString => throw new NotImplementedException();

        string IClientInfo.AutoimportDirectory { get; set; }

        [ImportingConstructor]
        public PrintHandler(IObjectsRepository repository, IObjectModifier modifier, IFileSaver filesaver, IClientInfo clientInfo,
            IPilotDialogService dialogService, IPersonalSettings personalSettings)
        {
            _repository = repository;
            _modifier = modifier;
            _filesaver = filesaver;
            _clientinfo = clientInfo;
            _personalSettings = personalSettings;
            _personalSettings.SubscribeSetting(SettingsFeatureKeys.FolderSettingKey).Subscribe(this);
            currentUser = _repository.GetCurrentPerson();

            var convertFromString = ColorConverter.ConvertFromString(dialogService.AccentColor);
            if (convertFromString != null)
            {
                var accentColor = (Color)convertFromString;
                ColorScheme.Initialize(accentColor, dialogService.Theme);
            }

        }


        public bool Handle(IPrintedDocumentInfo printTicket)
        {

            if (!currentUser.IsAdmin)
            {

                _types = _repository.GetTypes().ToDictionary(k => k.Id, v => v);
                var openTypes = _types.Values.FirstOrDefault(x =>
                    x.Name.Equals(DEPARTMENT_FOLDER_TYPE, StringComparison.OrdinalIgnoreCase));
                //проверка, создан ли нужный тип в базе
                if (openTypes == null) MessageBox.Show("Не найден тип: " + DEPARTMENT_FOLDER_TYPE);
                var openTypesID = openTypes.Id;
                openTypesIdcInts.Add(openTypesID);


                var message = new StringBuilder();
                message.AppendLine("Вам не разрешена печать электронных подлинников.");
                message.Append("Cделайте электронную копию для своего отдела ");
                message.AppendLine("через контекстное меню и отправьте на печать");
                //message.Append(printTicket.DocumentId);
                MessageBox.Show(message.ToString());
                return true;
            }


            /*Task.Factory.StartNew(async () => 
            {
                var loader = new ObjectLoader(_repository);
                var obj = await loader.Load(printTicket.DocumentId);
                var mess = new StringBuilder();
                foreach (var file in obj.Files)
                {
                    mess.AppendLine("Name " + file.Name);
                }

                MessageBox.Show(mess.ToString(), "Files to print", MessageBoxButton.OK, MessageBoxImage.Information);
              
            });*/

            return false;
        }

        async public Task FileSaveAsync(String path, String tempPath)
        {
            await Task.Delay(2000);
            if (File.Exists(path))
            {
                MessageBox.Show("Файл автоимпорта уже существует");
            }
            File.Copy(tempPath, path);
            //MessageBox.Show("Скопировали в автоимпорт");

        }

        public string getFileName(IDataObject selectedDataObject)
        {
            var filename_string = selectedDataObject.Files.FirstOrDefault()?.Name + ".xps";
            string[] operators = { ".pdf", ".dwfx", ".xps", ".jpeg", ".tiff", ".png", ".bmp", ".eml", ".msg" };
            foreach (IFile file in selectedDataObject.Files)
            {
                bool check_string = operators.Any(x => file.Name.ToLower().EndsWith(x));
                if (check_string) return file.Name;
            }
            return filename_string;
        }

        async public void FileSave(IDataObject selectedDataObject)
        {
            // каталог для сохранения файлов
            var folder = _clientinfo.AutoimportDirectory;
            // установить опцию "вшивать графические слои"
            var fsOptions = new FileSaverOptions();
            fsOptions.GraphicLayerOption = GraphicLayerOption.ForceInject;

            // получить имя файла

            var filename_string = getFileName(selectedDataObject);

            var filename = StrEditor(filename_string,
                (selectedDataObject.PreviousFileSnapshots.Count + 1).ToString());


            if (filename != null)
            {
                var path = Path.Combine(folder, filename);

                var tempDir = Path.Combine(System.IO.Path.GetTempPath(), UserTempDir);
                var tempPath = Path.Combine(tempDir, filename);
                if (File.Exists(tempPath))
                {
                    MessageBox.Show("Файл временного сохранения уже существует");
                }
                _filesaver.SaveFile(selectedDataObject, tempPath, fsOptions);
                //MessageBox.Show("Сохранили временно");
                await Task.Run(() => FileSaveAsync(path, tempPath));
            }
        }
        public void AddVersionLayer(IDataObject selectedDataObject, IObjectModifier modifier)
        {
            var version = selectedDataObject.PreviousFileSnapshots.Count.ToString();
            var Label = XamlStart + "версия документа: " + version + XamlClose;

            var elementId = Guid.NewGuid();
            var builder = modifier.Edit(selectedDataObject);
            using (var textBlocksStream = new MemoryStream())
            using (var writer = new StreamWriter(textBlocksStream))
            {
                writer.Write(Label);
                writer.Flush();

                var name = GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT + elementId;
                var element = new GraphicLayerElement(
                    elementId,
                    Guid.NewGuid(),
                    25,
                    25,
                    new Point(1, 1),
                    0, // надпись располагается под этим углом
                    0,
                    0,
                    VerticalAlignment.Bottom,
                    HorizontalAlignment.Left,
                    GraphicLayerElementConstants.XAML,
                    false);

                var serializer = new XmlSerializer(typeof(GraphicLayerElement));
                using (var stream = new MemoryStream())
                {
                    serializer.Serialize(stream, element);
                    builder.AddFile(name, stream, DateTime.Now, DateTime.Now, DateTime.Now);

                }

                builder.AddFile(GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT_CONTENT + element.ContentId,
                    textBlocksStream, DateTime.Now, DateTime.Now, DateTime.Now);
            }
        }

        private string StrEditor(string filename, string versNumberStr)
        {
            var versName = filename.Insert(filename.Length - 4, "PILOTREV" + versNumberStr);

            return versName;
        }
        #region Menu

        public void BuildMenu(IMenuHost menuHost)
        {
            var menuItem = menuHost.GetItems().First();
            menuHost.AddSubItem(menuItem, "miMySettings", "Настройки копирования", null, 2);
            if (currentUser.IsAdmin)
            {
                menuHost.AddSubItem(menuItem, "clear", "Очистить папку копий", null, 3);
            }
        }
        public void BuildContextMenu(IMenuHost menuHost, IEnumerable<IDataObject> selection, bool isContext)
        {
            if (isContext || selection == null)
                return;
            _selectedObjects = selection.ToList();
            _xpsDoc = selection.FirstOrDefault();

            if (_xpsDoc != null)
                menuHost.AddItem(MAKE_COPY, "Сделать копию для отдела", null, 0);
        }

        public void OnMenuItemClick(string itemName)
        {
            if (itemName == MAKE_COPY)
            {
                DeleteWasteFiles();
                ClearCopies(DateTime.Today.DayOfYear - 1);
                if (_selectedObjects.FirstOrDefault() != null)
                {
                    foreach (var obj in _selectedObjects)
                    {

                        FileSave(obj);
                        autoimportCopyFlag = true;
                    }
                }
            }
            if (itemName == "miMySettings")
            {
                var model = new SettingsViewModel(_personalSettings, _repository);
                var settingsView = new SettingsDialog() { DataContext = model };
                settingsView.ShowDialog();
            }

            if (itemName == "clear")
            {
                // Удаление за вчера и ранее
                ClearCopies(DateTime.Today.DayOfYear);
            }

        }

        public List<IDataObject> SelectionFamilyList(List<IDataObject> selectedDataObjectsObjects)
        {// формирует коллекцию объектов, содержащейся в папке
            var selectionWithChildren = new List<IDataObject>();
            if (selectedDataObjectsObjects != null)
            {
                foreach (var obj in selectedDataObjectsObjects)
                {
                    if (obj.Children == null)
                    {
                        selectionWithChildren.Add(obj);
                    }
                    else
                    {
                        foreach (var child in obj.Children)
                        {
                            selectionWithChildren.Add(obj);
                        }

                    }
                }
            }

            return selectionWithChildren;
        }

        #region Реализации интерфейсов

        public IList<string> SaveFile(IDataObject dataObject, string outputFilePath)
        {
            throw new NotImplementedException();
        }
        public IList<string> SaveFile(IDataObject dataObject, string outputFilePath, FileSaverOptions options)
        {
            throw new NotImplementedException();
        }
        public ClientType GetClientType()
        {
            throw new NotImplementedException();
        }
        public void Dispose()
        {

        }
        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }
        #endregion
        public void DeleteWasteFiles()
        {
            var wasteDir = Path.Combine(System.IO.Path.GetTempPath(), UserTempDir);
            if (Directory.Exists(wasteDir))
            {
                List<string> xpsFileList = Directory.GetFiles(wasteDir, "*.xps", SearchOption.AllDirectories).ToList();
                foreach (var f in xpsFileList)
                {
                    File.Delete(f);
                }
            }
        }
        public void OnLeftMouseButtonClick(XpsRenderClickPointContext pointContext)
        {
            MessageBox.Show("Клик левой кнопкой");
        }
        #endregion
        // удаление объектов из папки отдела объектов, созданных ранее указанного дня года
        public async void ClearCopies(int periodInt)
        {
            if (_dirtyFolderGUIDStr != null)
            {
                dirtyFolderGuid = Guid.Parse(_dirtyFolderGUIDStr);
                var loader = new ObjectLoader(_repository);
                var dirtyFolder = await loader.Load(dirtyFolderGuid); // загрузка папки отделов
                var dirtyIDList = new List<Guid>(); // объявление списка на удаление
                for (int i = 0; i < dirtyFolder.Children.Count; i++)
                {
                    var ch = await loader.Load(dirtyFolder.Children[i]); // загрузка объектов в папке
                    if (ch.Created.DayOfYear < periodInt) // проверка даты создания объекта: в какой день года
                    {
                        dirtyIDList.Add(dirtyFolder.Children[i]);
                    }
                }
                foreach (var g in dirtyIDList) // удаление по ID объектов из списка
                {
                    _modifier.DeleteById(g);
                }
                _modifier.Apply();
            }
            else { MessageBox.Show("Ошибка идентификатора папки автокопирования"); }

        }
        public void OnNext(KeyValuePair<string, string> value)
        {
            if (value.Value == null)
                return;
            if (value.Key == SettingsFeatureKeys.FolderSettingKey)
            {
                _dirtyFolderGUIDStr = value.Value;
            }
            ClearCopies(DateTime.Today.DayOfYear);
        }


    }
}