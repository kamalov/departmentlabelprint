﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Xml.Serialization;
using Microsoft.Win32;

namespace Ascon.Pilot.SDK.PrinterSample
{
    [Export(typeof(IAutoimportHandler))]
    [Export(typeof(IObjectChangeProcessor))]
    
    public class Autoimport : IAutoimportHandler, IObjectChangeProcessor, IFileSaver, IClientInfo, 
        IObserver<KeyValuePair<string, string>>, IObserver<IDataObject>
    {
        private readonly IObjectModifier _modifier;
        private readonly IDictionary<int, IType> _types;
        private Guid folderGuid;
        private string _autocopyFolderGUIDStr;
        private readonly IObjectsRepository _repository;
        const string DEPARTMENT_FOLDER_TYPE = "departmentFolder";
        private const string DOC_TYPE = "document";
        public const string XamlStart = "<TextBlock Foreground=\"Black\" FontSize=\"16\">КОПИЯ. ";
        public const string XamlClose = "</TextBlock>";
        public const string RevClue = "PILOTREV";
        public const string RevTitleBlock = ".ВЕРСИЯ ";
        private string parentUnitTitleStr;
        private readonly List<int> openTypesIdcInts = new List<int>();
        private readonly IFileSaver _filesaver;
        private IDataObject newObjectToSave;

        delegate void ViverHandler(string fname);

        //Событие вызывается при сохранении файла
        private event ViverHandler OnFileSaved;
        private TaskCompletionSource<bool> eventHandled;

        public const string UserTempDir = "departmentPrint";
        public string _revisionStr;

        public bool departmentcopyFlag;
        public IObjectBuilder objectBulder;

        public string AutoimportDirectory
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        public Uri ConnectionString => throw new NotImplementedException();

        [ImportingConstructor]
        public Autoimport(IObjectModifier modifier, IPilotDialogService dialogService, IObjectsRepository repository,
            IFileSaver filesaver, IClientInfo clientInfo, IPersonalSettings personalSettings)
        {
            _modifier = modifier;
            _filesaver = filesaver;
            _repository = repository;

            //var readSettings = new SettingsViewModel(_personalSettings, repository);
            //подписаться на настройки. Данные по настройке отдаются в через уведомление в метод OnNext()
            personalSettings.SubscribeSetting(SettingsFeatureKeys.FolderSettingKey).Subscribe(this);

            _types = repository.GetTypes().ToDictionary(k => k.Id, v => v);
            var openTypes = _types.Values.FirstOrDefault(x =>
                x.Name.Equals(DEPARTMENT_FOLDER_TYPE, StringComparison.OrdinalIgnoreCase));
            //проверка, создан ли нужный тип в базе
            if (openTypes == null) MessageBox.Show("Не найден тип: " + DEPARTMENT_FOLDER_TYPE);
            var openTypesID = openTypes.Id;
            openTypesIdcInts.Add(openTypesID);

            //обработчик события сохранения файла
            OnFileSaved += Autoimport_OnFileSaved;
        }

        private async  void Autoimport_OnFileSaved(string fname)
        {
           await Task.Factory.StartNew(async () =>
            {
                using (var vProcess = new Process())
                    try
                    {
                        var argument0 = "\"" + fname + "\"";
                        vProcess.StartInfo.FileName = "c:\\Program Files\\DblClickLocker\\DblClickLocker.exe";
                        vProcess.StartInfo.Arguments = argument0+ " " + "proceed"; 
                        vProcess.EnableRaisingEvents = true;
                        vProcess.Exited += VProcess_Exited;
                        vProcess.Start();

                        //vProcess.WaitForExit();
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.ToString());

                    }
            });
           
        }

        public async Task ViewTask(string filename)
        {
            eventHandled = new TaskCompletionSource<bool>();
            using (var vProcess = new Process())
              try
              {
                  vProcess.StartInfo.FileName = filename;
                  vProcess.EnableRaisingEvents = true;
                  vProcess.Exited += VProcess_Exited;
                  vProcess.Start();

                  //vProcess.WaitForExit();
              }
              catch (Exception e)
              {
                  MessageBox.Show(e.ToString());

              }
            
          await Task.WhenAny(eventHandled.Task);

            /*
            ViewerProcess xpsViwerProvProcess = new ViewerProcess(filename);
            await xpsViwerProvProcess.ViewDocTask(filename);*/
        }

        private void VProcess_Exited(object sender, EventArgs e)
        {
            eventHandled.TrySetResult(true);
            MessageBox.Show(sender.ToString() + "вьювер вышел");
        }

        //Функция обрабатывает автоимпорт. Запускается, когда файл попадает в папку автоимпорта
        public bool Handle(string filePath, string sourceFilePath, AutoimportSource autoimportSource)
        {
           // MessageBox.Show("Пуск обработки автоимпорта");
            if (!filePath.EndsWith("xps")| autoimportSource != AutoimportSource.UserFolder) return false;
            try
            {
                if (_autocopyFolderGUIDStr != null )
                {
                    folderGuid = Guid.Parse(_autocopyFolderGUIDStr);//selection.First().Id;
                }
                else { MessageBox.Show("Ошибка идентификатора папки автокопирования"); }

                _revisionStr = SearchRevision(filePath);
                var message = "Auto-imported from " + Localize(autoimportSource);
                var docIType = _types.Values.FirstOrDefault(x =>
                    x.Name.Equals(DOC_TYPE, StringComparison.OrdinalIgnoreCase));

                objectBulder = _modifier.Create(folderGuid, docIType);
                objectBulder
                    .SetAttribute("name", "AutoimportedDocument")
                    .SetAttribute("document_type", "Чертеж")
                    .AddFile(filePath);
               
                CopyFlagSet();
                try
                {
                    _modifier.Apply(); // и тут запускается обработчик изменений ProcessChange --- возникает ошибка System.NullReferenceException: Ссылка на объект не указывает на экземпляр объекта
                    // сохраненный файл открывается ассоцированной программой - Pilot XPS Viewer
                }
                catch (Exception e)
                {
                    
                }
               var route = FileSaveStirng(newObjectToSave);
               OnFileSaved?.Invoke(route);
               return true;
            }
            finally
            {
                departmentcopyFlag = false;
                File.Delete(filePath);
                try
                {
                    // _modifier.Delete(newObjectToSave);
                    _modifier.Delete(objectBulder.DataObject); // возможно, следует писать в отдельном потоке. 
                    _modifier.Apply(); // эта строка вызывает ошибку: Ссылка на объект не указывает на экземпляр объекта.
                }
                catch (Exception e)
                {

                }


            }
        }

        // в этот метод передаются данные по настройкам посредством уведомлений
        public void OnNext(KeyValuePair<string, string> value)
        {  
            if (value.Value == null)
                return;       
            if (value.Key == SettingsFeatureKeys.FolderSettingKey)
            {
                _autocopyFolderGUIDStr = value.Value;
            }
        }
        private string Localize(AutoimportSource autoimportSource)
        {
            switch (autoimportSource)
            {
                case AutoimportSource.Unknown:
                    return "Unknown";
                case AutoimportSource.PilotXps:
                    return "Pilot XPS printer";
                case AutoimportSource.UserFolder:
                    return "user auto-import directory";
                default:
                    throw new NotSupportedException();
            }
        }

        private void CopyFlagSet()
        {
            departmentcopyFlag = true;
        }

        private string SearchRevision(string filepath)
        {
            int indexOfrev = filepath.IndexOf(RevClue); // индекс первой буквы строки 'PILOTREV'. 
            int indexOfDot = filepath.Length - 4; // индекс точки - разделителя имени и расширения файла
            int indexOfClueEnd = indexOfrev + RevClue.Length;
            var revsyms = indexOfDot - indexOfClueEnd; // количество символов в обозначении ревизии
            var revindex = indexOfClueEnd;

            var revChar = new char[revsyms];
            filepath.CopyTo(revindex, revChar, 0, revsyms);
            string revisonNumStr = new string(revChar);
            return revisonNumStr;
        }

        public bool ProcessChanges(IEnumerable<DataObjectChange> changes, IObjectModifier modifier)
        {
            if (!departmentcopyFlag) return true;
            var currentPerson = _repository.GetCurrentPerson(); // получил текущего пользователя
            var currentUser = new Employee(currentPerson.Id, _repository);
            parentUnitTitleStr = currentUser.Depatrment;
            var changeNew = changes.FirstOrDefault()?.New;
            var stampStr = XamlStart + parentUnitTitleStr + RevTitleBlock +_revisionStr + XamlClose;
            AddGraphicLayer(changeNew, modifier, stampStr);

            newObjectToSave = changeNew;
            
           return true;
        }

    
        private void AddGraphicLayer(IDataObject dataObject, IObjectModifier modifier, string Label)
        {

            var elementId = Guid.NewGuid();
            var builder = modifier.Edit(dataObject);
            using (var textBlocksStream = new MemoryStream())
            using (var writer = new StreamWriter(textBlocksStream))
            {
                writer.Write(Label);
                writer.Flush();

                var name = GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT + elementId;
                var element = new GraphicLayerElement(
                    elementId,
                    Guid.NewGuid(),
                    15,
                    15,
                    new Point(1, 1),
                    -90, // надпись располагается под этим углом
                    0,
                    0,
                    VerticalAlignment.Bottom,
                    HorizontalAlignment.Left,
                    GraphicLayerElementConstants.XAML,
                    false);

                var serializer = new XmlSerializer(typeof(GraphicLayerElement));
                using (var stream = new MemoryStream())
                {
                    serializer.Serialize(stream, element);
                    builder.AddFile(name, stream, DateTime.Now, DateTime.Now, DateTime.Now);
                
                }

                builder.AddFile(GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT_CONTENT + element.ContentId,
                    textBlocksStream, DateTime.Now, DateTime.Now, DateTime.Now);
                }
        }

        public String FileSaveStirng(IDataObject selectedDataObject)
        {
         

            // каталог для сохранения файлов
            var curDir = Path.Combine(System.IO.Path.GetTempPath(), UserTempDir);

            if (!Directory.Exists(curDir))
            {
                Directory.CreateDirectory(curDir);
            }
           // установить опцию "вшивать графические слои"
            var fsOptions = new FileSaverOptions();
            fsOptions.GraphicLayerOption = GraphicLayerOption.ForceInject;
            // получить имя файла если нужно через графическое окно
            /*
                 var filename = selectedDataObject.Files.FirstOrDefault()?.Name;
                 if (filename != null)
                 {
                     var sfdialog = new SaveFileDialog();
                     sfdialog.DefaultExt = "xps";
                     sfdialog.AddExtension = true;
                     sfdialog.Title = "Укажите каталог для сохранения";
                     sfdialog.FileName = filename;
                     if (sfdialog.ShowDialog() == true)
                     {
                         pathStr = sfdialog.FileName;
                     }
            } */
            var filename = selectedDataObject.Files.FirstOrDefault().Name;
            var pathStr = Path.Combine(curDir, filename);
            var f = _filesaver.SaveFile(selectedDataObject, pathStr, fsOptions);
            return pathStr;
        }

        

        #region 

        // функции требуются для реализации интерфейсов
        public ClientType GetClientType()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
           // throw new NotImplementedException();
        }

        public IList<string> SaveFile(IDataObject dataObject, string outputFilePath)
        {
            throw new NotImplementedException();
        }

        public IList<string> SaveFile(IDataObject dataObject, string outputFilePath, FileSaverOptions options)
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            
        }

        public void OnCompleted()
        {
            
        }

        public void OnNext(IDataObject value)
        {
            
        }
#endregion
    }
}

