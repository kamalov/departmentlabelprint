﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

namespace Ascon.Pilot.SDK.PrinterSample
{
    class SettingsFeatureEditorViewModel2 : INotifyPropertyChanged
    {
        private readonly ISettingValueProvider _valueProvider;
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly List<string> _items = new List<string>();
        public string folderGUIDString;

        public SettingsFeatureEditorViewModel2(ISettingValueProvider valueProvider)
        {
            _valueProvider = valueProvider;
           folderGUIDString = valueProvider.GetValue();
        }

        public string InputFolderGUID
        {
            get { return folderGUIDString; }
            set
            {
                //это отправка значений настройки в репозиторий
                folderGUIDString = value;
                _valueProvider.SetValue(value);
                NotifyPropertyChanged("folderGUIDString");
            }
        }

        public List<string> Items
        {
            get { return _items; }
        }

        public string FolderGUID
        {
            get
            {
                return folderGUIDString;
            }
            set { folderGUIDString = value; }
        }

        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
