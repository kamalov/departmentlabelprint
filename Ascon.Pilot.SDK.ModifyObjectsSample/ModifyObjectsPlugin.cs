﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using Ascon.Pilot.SDK.Menu;
using Ascon.Pilot.SDK.CreateObjectSample;

namespace Ascon.Pilot.SDK.ModifyObjectsSample
{
    [Export(typeof(IMenu<ObjectsViewContext>))]
    public class ModifyObjectsPlugin : IMenu<ObjectsViewContext>
    {
        private readonly IObjectModifier _modifier;
        private readonly IObjectsRepository _repository;
        private const string CREATE_COPY_ITEM_NAME = "CreateCopyItemName";
        private IDataObject _selected;

        [ImportingConstructor]
        public ModifyObjectsPlugin(IObjectModifier modifier, IObjectsRepository repository)
        {
            _modifier = modifier;
            _repository = repository;
        }

        public void Build(IMenuBuilder builder, ObjectsViewContext context)
        {
            if (context.IsContext)
                return;

            var dataObjects = context.SelectedObjects.ToArray();
            if (dataObjects.Count() != 1)
                return;

            var itemNames = builder.ItemNames.ToList();
            const string indexItemName = "miCut";
            var insertIndex = itemNames.IndexOf(indexItemName) + 1;

            _selected = dataObjects.FirstOrDefault();
            builder.AddItem(CREATE_COPY_ITEM_NAME, insertIndex).WithHeader("Co_py");
        }

        public async void OnMenuItemClick(string name, ObjectsViewContext context)
        {
            if (name == CREATE_COPY_ITEM_NAME)
            {
                var loader = new ObjectLoader(_repository);
                var parent = await loader.Load(_selected.ParentId);
                var builder = _modifier.Create(parent, _selected.Type);
                foreach (var attribute in _selected.Attributes)
                {
                    if (attribute.Value is string)
                        builder.SetAttribute(attribute.Key, (string) attribute.Value);
                    if (attribute.Value is int)
                        builder.SetAttribute(attribute.Key, (int) attribute.Value);
                    if (attribute.Value is double)
                        builder.SetAttribute(attribute.Key, (double) attribute.Value);
                    if (attribute.Value is DateTime)
                        builder.SetAttribute(attribute.Key, (DateTime) attribute.Value);
                }
                _modifier.Apply();
            }
        }
    }
}
