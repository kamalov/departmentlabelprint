﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.Commands;

namespace CommandInvokerSample
{
    [Export(typeof(IFileHandler))]
    public class InvokerPdfToXpsImpl : IFileHandler
    {
        private readonly ICommandInvoker _commandInvoker;

        [ImportingConstructor]
        public InvokerPdfToXpsImpl(ICommandInvoker commandInvoker)
        {
            _commandInvoker = commandInvoker;
        }

        public bool Handle(string inputFile, out List<string> outputFiles, FileHandlerSource source)
        {
            outputFiles = new List<string>();

            if (!File.Exists(inputFile))
                return false;

            if (!string.Equals(Path.GetExtension(inputFile), ".pdf", StringComparison.OrdinalIgnoreCase))
                return false;

            // The convert command id from extension PDF2XPS.
            var convertPdfToXpsCommandId = new Guid("AFDEAC4F-3DA8-4CEB-B77B-2C956B7923B4");
            var pathToPdfFileArgs = Encoding.Unicode.GetBytes(inputFile);
            var xpsFileBytes = _commandInvoker.Invoke(convertPdfToXpsCommandId, pathToPdfFileArgs);
            if (xpsFileBytes == null)
                return false;

            var xpsFile = Encoding.Unicode.GetString(xpsFileBytes);
            Process.Start(xpsFile);
            return true;
        }
    }
}
