﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.Commands;
using Ascon.Pilot.SDK.Menu;

namespace CommandInvokerSample
{
    [Export(typeof(IMenu<ObjectsViewContext>))]
    class ContextMenuImpl : IMenu<ObjectsViewContext>
    {
        private readonly ICommandInvoker _commandInvoker;

        [ImportingConstructor]
        public ContextMenuImpl(ICommandInvoker commandInvoker)
        {
            _commandInvoker = commandInvoker;
        }

        public void Build(IMenuBuilder builder, ObjectsViewContext context)
        {
            builder.AddItem("CallFirstCommand", 0)
                .WithHeader("Call first command from CommandHandlerSample extension");

            builder.AddItem("CallSecondCommand", 1)
                .WithHeader("Call second command from CommandHandlerSample extension with params");

            builder.AddItem("CallSecondCommand2", 2)
                .WithHeader("Call second command from CommandHandlerSample extension without params");
        }

        public void OnMenuItemClick(string name, ObjectsViewContext context)
        {
            if (name == "CallFirstCommand")
            {
                var commandId = new Guid("2039E6FB-46AF-4CF8-B422-45E589FFF6ED");
                InvokeCommand(commandId, "Param1");
                return;
            }

            if (name == "CallSecondCommand")
            {
                var commandId = new Guid("BDF59966-590A-47BE-802D-7740CDCC1C5B");
                InvokeCommand(commandId, "Param 2");
                return;
            }

            if (name == "CallSecondCommand2")
            {
                var commandId = new Guid("BDF59966-590A-47BE-802D-7740CDCC1C5B");
                InvokeCommand(commandId, null);
            }
        }

        private void InvokeCommand(Guid commandId, string param)
        {
            try
            {
                byte[] bytes;
                if (param == null)
                {
                    bytes = _commandInvoker.Invoke(commandId, null);
                }
                else
                {
                    var args = Encoding.Unicode.GetBytes(param);
                    bytes = _commandInvoker.Invoke(commandId, args);
                }

                var messageFromOtherExtension = Encoding.Unicode.GetString(bytes);
                MessageBox.Show(messageFromOtherExtension);
            }
            catch (CommandHandlerNotFoundException e)
            {
                MessageBox.Show($"Command handler for command {e.CommandId} not found", "CommandInvokerSample");
            }
            
        }
    }
}
