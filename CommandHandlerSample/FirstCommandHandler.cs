﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ascon.Pilot.SDK.Commands;

namespace CommandHandlerSample
{
    [Export(typeof(ICommandHandler))]
    public class FirstCommandHandler : ICommandHandler
    {
        public Guid CommandId => new Guid("2039E6FB-46AF-4CF8-B422-45E589FFF6ED");
        public string Description => "First command description";

        public byte[] Handle(byte[] args)
        {
            if (args == null)
                return null;

            if (args.Length == 0)
                return null;

            var name = "CommandHandlerSample";
            var param1 = Encoding.Unicode.GetString(args);
            var answer = Encoding.Unicode.GetBytes($"Message from extension {name}. You have called first command {CommandId} with args: {param1}.");
            return answer;
        }
    }
}
