﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ascon.Pilot.SDK.Commands;

namespace CommandHandlerSample
{
    [Export(typeof(ICommandHandler))]
    public class SecondCommandHandler : ICommandHandler
    {
        public Guid CommandId => new Guid("BDF59966-590A-47BE-802D-7740CDCC1C5B");
        public string Description => "Second command description";

        public byte[] Handle(byte[] args)
        {
            var name = "CommandHandlerSample";
            var answer = Encoding.Unicode.GetBytes($"Message from extension {name}. You have called second command {CommandId} without args");
            if (args == null)
                return answer;

            if (args.Length == 0)
                return answer;
            
            var param1 = Encoding.Unicode.GetString(args);
            answer = Encoding.Unicode.GetBytes($"Message from extension {name}. You have called second command {CommandId} with args: {param1}.");
            return answer;
        }
    }
}
