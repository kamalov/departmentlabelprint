﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Xml.Serialization;
using Ascon.Pilot.SDK.Menu;
using Ascon.Pilot.SDK.CreateObjectSample;
using Ascon.Pilot.Theme.ColorScheme;
using Color = System.Windows.Media.Color;
using Point = System.Windows.Point;

namespace Ascon.Pilot.SDK.GraphicLayerSample
{
    [Export(typeof(IMenu<MainViewContext>))]
    [Export(typeof(IMenu<XpsRenderClickPointContext>))]
    public class GraphicLayerSample : IMenu<MainViewContext>, IHandle<UnloadedEventArgs>, IObserver<INotification>, IMenu<XpsRenderClickPointContext>
    {
        private const string ServiceGraphicLayerMenu = "ServiceGraphicLayerMenu";
        private readonly IObjectModifier _modifier;
        private readonly IObjectsRepository _repository;
        private readonly IXpsViewer _xpsViewer;

        private IPerson _currentPerson;
        private GraphicLayerElementSettingsView _settingsView;
        private GraphicLayerElementSettingsModel _model;
        private VerticalAlignment _verticalAlignment;
        private HorizontalAlignment _horizontalAlignment;

        private string _filePath = string.Empty;
        private double _xOffset;
        private double _yOffset;
        private double _scaleXY;
        private double _angle;
        private bool _includeStamp;

        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [ImportingConstructor]
        public GraphicLayerSample(IEventAggregator eventAggregator, IObjectModifier modifier, IObjectsRepository repository, IPilotDialogService dialogService, IXpsViewer xpsViewer)
        {
            var convertFromString = ColorConverter.ConvertFromString(dialogService.AccentColor);
            if (convertFromString != null)
            {
                var color = (Color)convertFromString;
                ColorScheme.Initialize(color, dialogService.Theme);
            }
            eventAggregator.Subscribe(this);
            _modifier = modifier;
            _repository = repository;
            _xpsViewer = xpsViewer;
            var signatureNotifier = repository.SubscribeNotification(NotificationKind.ObjectSignatureChanged);
            var fileNotifier = repository.SubscribeNotification(NotificationKind.ObjectFileChanged);
            signatureNotifier.Subscribe(this);
            fileNotifier.Subscribe(this);
            CheckSettings();
        }

        public void Build(IMenuBuilder builder, MainViewContext context)
        {
            var menuItem = builder.ItemNames.First();
            builder.GetItem(menuItem).AddItem(ServiceGraphicLayerMenu, 0).WithHeader(Properties.Resources.txtMenuItem);
        }

        public void OnMenuItemClick(string itemName, MainViewContext context)
        {
            var x = _xOffset.ToString(CultureInfo.InvariantCulture);
            var y = _yOffset.ToString(CultureInfo.InvariantCulture);
            var scale = _scaleXY.ToString("N1");
            var angle = _angle.ToString(CultureInfo.InvariantCulture);
            var includeStamp = _includeStamp;

            _model = new GraphicLayerElementSettingsModel(_filePath, x, y, scale, angle, _verticalAlignment, _horizontalAlignment, includeStamp);
            _model.OnSaveSettings += ReloadSettings;

            if (itemName != ServiceGraphicLayerMenu)
                return;

            _settingsView = new GraphicLayerElementSettingsView { DataContext = _model };

            var activeWindowHandle = GetForegroundWindow();
            new WindowInteropHelper(_settingsView)
            {
                Owner = activeWindowHandle
            };
            _settingsView.ShowDialog();
            System.Windows.Threading.Dispatcher.Run();
        }

        private void ReloadSettings(object sender, EventArgs e)
        {
            CheckSettings();
        }

        private void CheckSettings()
        {
            var path = Properties.Settings.Default.Path;
            _filePath = path;
            _includeStamp = Properties.Settings.Default.IncludeStamp;
            double.TryParse(Properties.Settings.Default.X, out _xOffset);
            double.TryParse(Properties.Settings.Default.Y, out _yOffset);
            _scaleXY = 1;
            try
            {
                var tmp = Properties.Settings.Default.Scale.Split(',', '.');
                double whole;
                double.TryParse(tmp[0], out whole);
                double fraction = 0;
                if (tmp.Length > 1)
                    double.TryParse("0" + CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator + tmp[1], out fraction);
                _scaleXY = whole + fraction;
            }
            catch (Exception) { }

            double.TryParse(Properties.Settings.Default.Angle, out _angle);
            Enum.TryParse(Properties.Settings.Default.VerticalAligment, out _verticalAlignment);
            Enum.TryParse(Properties.Settings.Default.HorizontalAligment, out _horizontalAlignment);
        }

        private void SaveToDataBaseRastr(IDataObject dataObject, string facsimileFileName)
        {
            if (string.IsNullOrEmpty(_filePath))
                return;
            var builder = _modifier.Edit(dataObject);
            using (var fileStream = File.Open(_filePath, FileMode.Open, FileAccess.ReadWrite))
            {
                var positionId = _currentPerson.MainPosition.Position;
                var byteArray = new byte[fileStream.Length];
                fileStream.Read(byteArray, 0, (int)fileStream.Length);
                var imageStream = new MemoryStream(byteArray);
                var scale = new Point(_scaleXY, _scaleXY);

                var element = GraphicLayerElementCreator.Create(_xOffset, _yOffset, scale, _angle, positionId, _verticalAlignment,
                    _horizontalAlignment, GraphicLayerElementConstants.BITMAP, ToGuid(_currentPerson.Id), 0, true);
                var serializer = new XmlSerializer(typeof(GraphicLayerElement));
                using (var stream = new MemoryStream())
                {
                    serializer.Serialize(stream, element);
                    builder.AddFile(facsimileFileName, stream, DateTime.Now, DateTime.Now, DateTime.Now);
                    builder.AddFile(GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT_CONTENT + element.ContentId, imageStream, DateTime.Now, DateTime.Now, DateTime.Now);
                }
                _modifier.Apply();
            }
        }

        private void SaveToDataBaseXaml(IDataObject dataObject, string xamlObject, Guid elementId)
        {
            var builder = _modifier.Edit(dataObject);
            var textBlocksStream = new MemoryStream();
            using (var writer = new StreamWriter(textBlocksStream))
            {
                writer.Write(xamlObject);
                writer.Flush();

                var positionId = _currentPerson.MainPosition.Position;
                var name = GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT + elementId + "_" + positionId;
                var scale = new Point(_scaleXY, _scaleXY);

                var element = GraphicLayerElementCreator.Create(_xOffset, _yOffset, scale, _angle, positionId, _verticalAlignment, _horizontalAlignment, GraphicLayerElementConstants.XAML, elementId, 0, true);
                var serializer = new XmlSerializer(typeof(GraphicLayerElement));
                using (var stream = new MemoryStream())
                {
                    serializer.Serialize(stream, element);
                    builder.AddFile(name, stream, DateTime.Now, DateTime.Now, DateTime.Now);
                }
                builder.AddFile(GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT_CONTENT + element.ContentId, textBlocksStream, DateTime.Now, DateTime.Now, DateTime.Now);
                _modifier.Apply();
            }
        }

        public async void OnNext(INotification value)
        {
            if (string.IsNullOrEmpty(_filePath))
                return;

            _currentPerson = _repository.GetCurrentPerson();
            if (value.ChangeKind == NotificationKind.ObjectSignatureChanged && _currentPerson.Id == value.UserId)
            {
                var loaderForFirstSign = new ObjectLoader(_repository);
                var obj = await loaderForFirstSign.Load(value.ObjectId);
                
                if (obj.Files.Any(f => f.Name.Contains("Signature")))
                    AddGraphicLayer(obj);
                
                return;
            }
            if (value.ChangeKind == NotificationKind.ObjectFileChanged && _currentPerson.Id == value.UserId)
            {
                var loader = new ObjectLoader(_repository);
                var obj = await loader.Load(value.ObjectId);
                
                if (!obj.Files.Any(f => f.Name.Contains("Signature")))
                    return;
                    
                AddGraphicLayer(obj);
                
            }
        }

        private void AddGraphicLayer(IDataObject dataObject)
        {
            var facsimileFileName = GraphicLayerElementConstants.GRAPHIC_LAYER_ELEMENT + ToGuid(_currentPerson.Id);
            if (!dataObject.Files.Any(f => f.Name.Equals(facsimileFileName)))
            {
                var signaturesCount = dataObject.Files.Count(f => f.Name.Contains("Signature"));
                var xpsFile = dataObject.ActualFileSnapshot.Files.FirstOrDefault(f =>
                {
                    var extension = Path.GetExtension(f.Name);
                    return extension != null && (extension.Equals(".xps") || extension.Equals(".dwfx"));
                });
                var requestsCount = xpsFile?.Signatures.Count;
                if (requestsCount == signaturesCount && _includeStamp)
                {
                    var stamp1 = GraphicLayerElementCreator.CreateStamp1().ToString();
                    SaveToDataBaseXaml(dataObject, stamp1, Guid.NewGuid());
                    var stamp2 = GraphicLayerElementCreator.CreateStamp2().ToString();
                    SaveToDataBaseXaml(dataObject, stamp2, Guid.NewGuid());
                }
                SaveToDataBaseRastr(dataObject, facsimileFileName);
            }
        }

        public static Guid ToGuid(int value)
        {
            byte[] bytes = new byte[16];
            BitConverter.GetBytes(value).CopyTo(bytes, 0);
            return new Guid(bytes);
        }

        public void Handle(UnloadedEventArgs message)
        {
            _model.OnSaveSettings -= ReloadSettings;
        }

        public void OnError(Exception error) { }
        public void OnCompleted() { }

        const string HIDE_MENU_NAME = "mHide";
        const string SHOW_MENU_NAME = "mShow";
        public void Build(IMenuBuilder builder, XpsRenderClickPointContext context)
        {
            var items = builder.ItemNames.ToList();
            var position = items.Count;
            builder.AddItem(HIDE_MENU_NAME, position++).WithHeader("Hide");
            builder.AddItem(SHOW_MENU_NAME, position).WithHeader("Show");
            
        }

        public void OnMenuItemClick(string name, XpsRenderClickPointContext context)
        {
            bool? hidden = null;
            if (name == HIDE_MENU_NAME)
            {
                hidden = true;
            }
            if (name == SHOW_MENU_NAME)
            {
                hidden = false;
            }
            if (hidden != null)
            {
                var elementId = ToGuid(_repository.GetCurrentPerson().Id);
                _xpsViewer.SetGraphicLayerElementsVisibility(new List<Guid> {elementId}, hidden.Value);
            }
        }
    }
}
