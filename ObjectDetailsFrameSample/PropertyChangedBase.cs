﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DocumentsExplorerDetailsViewSample
{
    public abstract class PropertyChangedBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool? _closeResult;

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool? CloseResult
        {
            get => _closeResult;
            private set
            {
                _closeResult = value;
                NotifyOfPropertyChange(nameof(CloseResult));
            }
        }

        protected virtual void NotifyOfAllPropertiesChanged()
        {
            NotifyOfPropertyChange(null);
        }

        protected void NotifyOfPropertyChange([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void CloseView(bool dialogResult)
        {
            CloseResult = dialogResult;
        }
    }
}