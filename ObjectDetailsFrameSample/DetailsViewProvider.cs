﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using Ascon.Pilot.SDK;

namespace DocumentsExplorerDetailsViewSample
{
    [Export(typeof(IDocumentsExplorerDetailsViewProvider))]
    public class DetailsViewProvider : IDocumentsExplorerDetailsViewProvider
    {
        [ImportingConstructor]
        public DetailsViewProvider(IObjectsRepository repository)
        {
            var sectionType = repository.GetType("section");
            Types = new List<IType>() { sectionType } ;
        }

        public FrameworkElement GetDetailsView(ObjectsViewContext context)
        {
            var obj = context.SelectedObjects.FirstOrDefault();
            if (obj == null)
                return null;
            var viewModel = new TypeDetailsViewModel(obj.Type);
            var view = new TypeDetailsView
            {
                DataContext = viewModel
            };
            return view;
        }

        public List<IType> Types { get; }
    }
}
