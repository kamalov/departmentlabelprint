﻿using System.Windows.Controls;

namespace DocumentsExplorerDetailsViewSample
{
    /// <summary>
    /// Interaction logic for DetailsView.xaml
    /// </summary>
    public partial class TypeDetailsView : UserControl
    {
        public TypeDetailsView()
        {
            InitializeComponent();
        }
    }
}
