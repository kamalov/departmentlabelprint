﻿using Ascon.Pilot.SDK;

namespace DocumentsExplorerDetailsViewSample
{
    class TypeDetailsViewModel : PropertyChangedBase
    {
        private string _name;
        private string _title;

        public TypeDetailsViewModel(IType type)
        {
            Name = type.Name;
            Title = type.Title;
        }

        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                NotifyOfPropertyChange();
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                NotifyOfPropertyChange();
            }
        }
    }
}
