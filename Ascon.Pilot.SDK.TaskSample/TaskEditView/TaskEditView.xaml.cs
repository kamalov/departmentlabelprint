﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Ascon.Pilot.SDK.TaskSample.TaskEditView
{
    /// <summary>
    /// Interaction logic for TaskEditView.xaml
    /// </summary>
    public partial class TaskEditView 
    {
        public TaskEditView()
        {
            InitializeComponent();
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Close()
        {
            if (Parent is Window window)
                window.Close();
        }
    }
}
