﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Ascon.Pilot.SDK.Controls;
using Ascon.Pilot.SDK.Controls.Commands;
using Ascon.Pilot.SDK.TaskSample.Services;
using Ascon.Pilot.Theme.Tools;
using Microsoft.Win32;

namespace Ascon.Pilot.SDK.TaskSample.TaskEditView.Attachments
{
    public class AttachmentsViewModel : PropertyChangedBase
    {
        private readonly IObjectsRepository _repository;
        private readonly IPilotDialogService _pilotDialogService;
        private readonly ITabServiceProvider _tabServiceProvider;
        private readonly DelegateCommand _attachObjectCommand;
        private readonly DelegateCommand _attachFileCommand;

        public AttachmentsViewModel(IObjectsRepository repository, IPilotDialogService pilotDialogService, ITabServiceProvider tabServiceProvider)
        {
            _repository = repository;
            _pilotDialogService = pilotDialogService;
            _tabServiceProvider = tabServiceProvider;
            _attachObjectCommand = new DelegateCommand(AddObjectAttachment);
            _attachFileCommand = new DelegateCommand(AddFileAttachment);
        }

        public event EventHandler<EventArgs> AttachmentsChanged;
        public ICommand AttachObjectCommand => _attachObjectCommand;
        public ICommand AttachFileCommand => _attachFileCommand;
        public ObservableCollection<TaskAttachment> Attachments { get; } = new ObservableCollection<TaskAttachment>();

        public void RemoveAttachment(Guid attachmentId)
        {
            var attachment = Attachments.FirstOrDefault(x => x.Id == attachmentId);
            if (attachment != null)
                Attachments.Remove(attachment);

            AttachmentsChanged?.Invoke(this, EventArgs.Empty);
        }

        public void ShowAttachment(Guid attachmentId)
        {
            var attachment = Attachments.FirstOrDefault(x => x.Id == attachmentId);
            if (attachment == null)
                return;

            _tabServiceProvider.ShowElement(attachmentId);
        }

        private void AddObjectAttachment()
        {
            var dialogOptions = _pilotDialogService
                .NewOptions()
                .WithParentWindow(Win32Service.GetActiveWindow())
                .WithCaption("Select attachments")
                .WithOkButtonCaption("Attach");

            var attachments = _pilotDialogService.ShowDocumentsSelectorDialog(dialogOptions);
            foreach (var attachment in attachments)
            {
                Attachments.Add(new TaskAttachment(attachment.Id, attachment.DisplayName, attachment.Type.Title, Theme.Icons.Instance.DocumentsIcon));
            }

            AttachmentsChanged?.Invoke(this, EventArgs.Empty);
        }

        private void AddFileAttachment()
        {
            var storageItems = GetSelectedFiles();
            foreach (var attachment in storageItems)
            {
                Attachments.Add(new TaskAttachment(attachment.DataObject.Id, attachment.DataObject.DisplayName, "file", Theme.Icons.Instance.FileIcon));
            }

            AttachmentsChanged?.Invoke(this, EventArgs.Empty);
        }

        public List<IStorageDataObject> GetSelectedFiles()
        {
            bool isValidPath;
            List<IStorageDataObject> storageItems;
            do
            {
                List<string> filePaths = null;
                string initialDirectory = _repository.GetStoragePath();
                var result = ShowOpenFileDialog("Select file attachments", null, true, initialDirectory, out filePaths);

                if (result != true)
                    return new List<IStorageDataObject>();

                storageItems = _repository.GetStorageObjects(filePaths).ToList();

                isValidPath = storageItems.All(x => x.State != StorageObjectState.None);

                if (!isValidPath)
                    MessageBox.Show("Only files from the pilot storage virtual drive can be attached.");

            } while (!isValidPath);

            return storageItems;
        }

        private bool? ShowOpenFileDialog(string title, string filter, bool mutiselect, string initialDirectory, out List<string> filePaths)
        {
            var dialog = new OpenFileDialog
            {
                Multiselect = mutiselect,
                Title = title
            };

            if (filter != null)
                dialog.Filter = filter;

            if (initialDirectory != null)
            {
                var fullPath = Path.GetFullPath(initialDirectory);
                dialog.InitialDirectory = fullPath;
            }

            try
            {
                return dialog.ShowDialog(LayoutHelper.GetOwnerWindow());
            }
            finally
            {
                filePaths = dialog.FileNames.ToList();
            }
        }
    }
}
