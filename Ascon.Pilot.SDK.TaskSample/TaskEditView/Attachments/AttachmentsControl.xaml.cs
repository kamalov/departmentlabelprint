﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ascon.Pilot.SDK.TaskSample.TaskEditView.Attachments
{
    /// <summary>
    /// Interaction logic for AttachmentsControl.xaml
    /// </summary>
    public partial class AttachmentsControl : UserControl
    {
        public AttachmentsControl()
        {
            InitializeComponent();
        }

        private void RemoveAttachmentMenuItemClick(object sender, RoutedEventArgs e)
        {
            var removeButton = (MenuItem)sender;
            if (removeButton.Tag is Guid attachmentId)
                ((AttachmentsViewModel) DataContext)?.RemoveAttachment(attachmentId);
        }

        private void ShowAttachmentItemClick(object sender, RoutedEventArgs e)
        {
            var button = (MenuItem)sender;
            if (button.Tag is Guid attachmentId)
                ((AttachmentsViewModel) DataContext)?.ShowAttachment(attachmentId);
        }
    }
}
