﻿using System;
using System.Windows.Media;
using Ascon.Pilot.SDK.Controls;

namespace Ascon.Pilot.SDK.TaskSample.TaskEditView.Attachments
{
    public class TaskAttachment : PropertyChangedBase
    {
        public TaskAttachment(Guid id, string title, string typeTitle, ImageSource icon)
        {
            Id = id;
            Icon = icon;
            Title = title;
            TypeTitle = typeTitle;
        }

        public Guid Id { get; }
        public string Title { get; }
        public string TypeTitle { get; }
        public ImageSource Icon { get; }

        public override bool Equals(object obj)
        {
            return obj is TaskAttachment other && other.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
