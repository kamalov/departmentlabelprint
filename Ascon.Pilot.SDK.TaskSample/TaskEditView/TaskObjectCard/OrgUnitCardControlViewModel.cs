﻿using Ascon.Pilot.SDK.Controls.ObjectCardView;

namespace Ascon.Pilot.SDK.TaskSample.TaskEditView.ObjectCard
{
    class ExecutorCardControlViewModel : OrgUnitCardControlViewModel
    {
        public ExecutorCardControlViewModel(IAttribute attribute, IType type, object initValue, bool isReadOnly, 
            IPilotDialogService dialogService, IObjectsRepository repository, IObjectCardAutoComplete autoComplete, 
            IAttributeFormatParser attributeFormatParser, bool editMode) 
            : base(attribute, type, initValue, isReadOnly, dialogService, repository, autoComplete, attributeFormatParser, editMode)
        {
        }

        protected override IPilotDialogOptions GetOrgUnitsSelectorDialogOptions()
        {
            return _dialogService.NewOptions()
                .WithParentWindow(GetActiveWindow())
                .WithCaption("Select executor")
                .WithOkButtonCaption("Select")
                .WithAllowMultiSelect(false);
        }
    }
}