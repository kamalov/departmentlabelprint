﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ascon.Pilot.SDK.Controls;
using Ascon.Pilot.SDK.Controls.ObjectCardView;
using Ascon.Pilot.SDK.TaskSample.Services;
using Ascon.Pilot.SDK.TaskSample.TaskEditView.Attachments;
using Ascon.Pilot.SDK.TaskSample.TaskEditView.ObjectCard;

namespace Ascon.Pilot.SDK.TaskSample.TaskEditView.TaskObjectCard
{
    public class TaskCardControlViewModel : PropertyChangedBase
    {
        private readonly IPilotServices _pilotServices;
        private readonly IInvalidateData _invalidateData;

        private ObjectCardViewModel _taskCardViewModel;
        private AttachmentsViewModel _attachmentsViewModel;

        public TaskCardControlViewModel(IType type, IPilotServices pilotServices, IInvalidateData invalidateData)
        {
            _pilotServices = pilotServices;
            _invalidateData = invalidateData;
            BuildCard(type);
        }

        public ObjectCardViewModel TaskCardViewModel
        {
            get => _taskCardViewModel;
            set
            {
                _taskCardViewModel = value;
                NotifyOfPropertyChange(nameof(TaskCardControl));
            }
        }

        public AttachmentsViewModel AttachmentsViewModel
        {
            get => _attachmentsViewModel;
            set
            {
                _attachmentsViewModel = value;
                NotifyOfPropertyChange(nameof(AttachmentsViewModel));
            }
        }

        protected virtual ObjectCardViewModel CreateObjectCardViewModel(IObjectsRepository repository, IPilotDialogService pilotDialogService, IAttributeFormatParser attributeFormatParser)
        {
            return new TaskObjectCardViewModel(_pilotServices.ObjectsRepository,_pilotServices.PilotDialogService, _pilotServices.AttributeFormatParser);
        }

        private void BuildCard(IType type)
        {
            TaskCardViewModel = CreateObjectCardViewModel(_pilotServices.ObjectsRepository, _pilotServices.PilotDialogService, _pilotServices.AttributeFormatParser);
            TaskCardViewModel.CreateCard(type);
            TaskCardViewModel.IsValidInputChanged += OnIsValidInputChanged;

            AttachmentsViewModel = new AttachmentsViewModel(_pilotServices.ObjectsRepository, _pilotServices.PilotDialogService, _pilotServices.TabServiceProvider);
            AttachmentsViewModel.AttachmentsChanged += OnAttachmentsChanged;
        }

        private void OnIsValidInputChanged(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void OnAttachmentsChanged(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void Invalidate()
        {
            _invalidateData.Invalidate();
        }
    }
}
