﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ascon.Pilot.SDK.Controls.ObjectCardView;

namespace Ascon.Pilot.SDK.TaskSample.TaskEditView.ObjectCard
{
    sealed class TaskObjectCardViewModel : ObjectCardViewModel
    {
        public TaskObjectCardViewModel(IObjectsRepository repository, IPilotDialogService dialogService, IAttributeFormatParser attributeFormatParser) 
            : base(repository, dialogService, attributeFormatParser)
        {
        }

        public override CardControlViewModel CreateCardControlViewModel(IAttribute attribute, IType type, object initValue, bool isEditMode, bool isReadOnlyAttribute)
        {
            switch (attribute.Type)
            {
                case AttributeType.OrgUnit:
                    if (attribute.Name == SystemTaskAttributes.EXECUTOR_POSITION)
                        return new ExecutorCardControlViewModel(attribute, type, initValue, isReadOnlyAttribute, _dialogService, _repository, this, _attributeFormatParser, isEditMode);

                    return new OrgUnitCardControlViewModel(attribute, type, initValue, isReadOnlyAttribute, _dialogService, _repository, this, _attributeFormatParser, isEditMode);

                default:
                    return new CardControlViewModel(attribute, type, initValue, isReadOnlyAttribute, _dialogService, _repository, this, _attributeFormatParser, isEditMode);
            }
        }
    }
}
