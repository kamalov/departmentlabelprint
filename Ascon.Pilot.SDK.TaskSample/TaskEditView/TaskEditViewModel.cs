﻿using System;
using System.Linq;
using System.Windows.Input;
using Ascon.Pilot.SDK.Controls;
using Ascon.Pilot.SDK.Controls.Commands;
using Ascon.Pilot.SDK.TaskSample.Extensions;
using Ascon.Pilot.SDK.TaskSample.Services;
using Ascon.Pilot.SDK.TaskSample.TaskEditView.TaskObjectCard;

namespace Ascon.Pilot.SDK.TaskSample.TaskEditView
{
    class TaskEditViewModel : PropertyChangedBase, IInvalidateData
    {
        private readonly DelegateCommand _submitCommand;
        private readonly IObjectModifier _modifier;
        private readonly IObjectsRepository _repository;

        public TaskEditViewModel(IType type, IPilotServices pilotServices)
        {
            Type = type;
            _modifier = pilotServices.ObjectModifier;
            _repository = pilotServices.ObjectsRepository;

            _submitCommand = new DelegateCommand(SubmitTask, CanSubmit);

            TaskCardControlViewModel = new TaskCardControlViewModel(type, pilotServices, this);
        }

        public TaskCardControlViewModel TaskCardControlViewModel { get; }
        public IType Type { get; }
        public ICommand SubmitCommand => _submitCommand;

        public void Invalidate()
        {
            _submitCommand.RaiseCanExecuteChanged();
        }

        private void SubmitTask()
        {
            var builder = _modifier.CreateTask(Type);
            var attributes = TaskCardControlViewModel.TaskCardViewModel.Values;

            foreach (var pair in attributes)
            {
                var attributeName = pair.Key;
                var value = pair.Value.SetToUniversalTimeIfNeeded();
                builder.SetAttributeValue(attributeName, value);
            }

            var person = _repository.GetCurrentPerson();
            builder.SetInitiator(person.MainPosition.Position);
            builder.SetAttachments(TaskCardControlViewModel.AttachmentsViewModel.Attachments.Select(a => a.Id), _modifier);
            builder.SetDeadline(attributes.TryGetValue(SystemTaskAttributes.DEADLINE_DATE, out var dValue) ? dValue.DateValue : null, false);

            _modifier.Apply();

            CloseView(true);
        }

        private bool CanSubmit()
        {
            return TaskCardControlViewModel.TaskCardViewModel.IsValidInput;
        }
    }
}
