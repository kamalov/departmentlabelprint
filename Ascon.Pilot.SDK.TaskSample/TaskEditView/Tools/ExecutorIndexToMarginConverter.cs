﻿using System;
using System.Windows.Data;

namespace Ascon.Pilot.SDK.TaskSample.TaskEditView.Tools
{
    class ExecutorIndexToMarginConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var index = values[0] as int?;
            var count = values[1] as int?;

            var result = index.HasValue && count.HasValue && count.Value - index.Value == 2;
            return result;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
