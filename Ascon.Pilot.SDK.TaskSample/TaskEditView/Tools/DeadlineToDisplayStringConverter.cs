﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;

namespace Ascon.Pilot.SDK.TaskSample.TaskEditView.Tools
{
    class DeadlineToDisplayStringConverter : MarkupExtension, IValueConverter
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is DateTime deadline))
                return null;

            if (deadline.Date == DateTime.MaxValue.Date)
                return null;

            var deadlineTime = deadline - deadline.Date;
            return deadline.ToString(deadlineTime == new TimeSpan(0, 0, 0, 0) ? "d" : "g");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
