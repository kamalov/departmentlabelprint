﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Markup;
using Ascon.Pilot.Theme;

namespace Ascon.Pilot.SDK.TaskSample.TaskEditView.Tools
{
    public class TaskStateToImageConverter : MarkupExtension, IValueConverter
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is State))
                throw new NotSupportedException();

            var state = (State)value;
            switch (state)
            {
                case State.Assigned:
                    return Icons.Instance.AssignedIcon;
                case State.Completed:
                    return Icons.Instance.CompletedIcon;
                case State.InProgress:
                    return Icons.Instance.InProgressIcon;
                case State.OnValidation:
                    return Icons.Instance.OnValidationIcon;
                case State.Revoked:
                    return Icons.Instance.RevokedIcon;
                case State.None:
                    return null;
            }

            throw new NotSupportedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
