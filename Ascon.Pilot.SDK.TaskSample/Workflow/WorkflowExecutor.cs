﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Ascon.Pilot.SDK.Controls.Commands;
using Ascon.Pilot.SDK.Controls.ObjectCardView;
using Ascon.Pilot.SDK.TaskSample.Services;

namespace Ascon.Pilot.SDK.TaskSample.Workflow
{
    public class WorkflowExecutor: OrgUnitItem
    {
        private readonly IPilotServices _pilotServices;
        private readonly IInvalidateData _invalidateData;
        private IType _taskType;
        private DateTime? _deadline;
        private readonly DelegateCommand<IType> _setTaskTypeCommand;
        private WorkflowTaskCardControlViewModel _cardControl;

        public WorkflowExecutor(IType type, 
            IPilotServices pilotServices, 
            IOrgUnitsListManager orgUnitsListManager, 
            int positionId, 
            IEnumerable<IType> availableTaskTypes, 
            IInvalidateData invalidateData)
            : base(pilotServices.ObjectsRepository, orgUnitsListManager, positionId)
        {
            _pilotServices = pilotServices;
            _invalidateData = invalidateData;
            AvailableTaskTypes = availableTaskTypes.Select(t => new TaskTypeItem(t)).ToList();
            CardControl = new WorkflowTaskCardControlViewModel(type, _pilotServices, _invalidateData);
            CardControl.TaskCardViewModel.IsValidInputChanged += OnIsValidInputChanged;
            TaskType = type;
            IsReadonly = false;

            _setTaskTypeCommand = new DelegateCommand<IType>(SetNewTaskType);
        }

        private void OnIsValidInputChanged(object sender, EventArgs e)
        {
            _invalidateData.Invalidate();
        }

        public ICommand SetTaskTypeCommand => _setTaskTypeCommand;

        public WorkflowTaskCardControlViewModel CardControl
        {
            get => _cardControl;
            private set
            {
                _cardControl = value;
                NotifyOfPropertyChange(nameof(CardControl));
            }
        }
        public bool IsReadonly { get; }
        public IList<TaskTypeItem> AvailableTaskTypes { get; }

        public IType TaskType
        {
            get => _taskType;
            set => SetNewTaskType(value);
        }

        public string DeadlineAttrTitle => TaskType?.Attributes.FirstOrDefault(x => x.Name == SystemTaskAttributes.DEADLINE_DATE)?.Title;

        public DateTime? Deadline
        {
            get => _deadline;
            set
            {
                _deadline = value;
                NotifyOfPropertyChange(nameof(Deadline));
            }
        }
    
        private void SetNewTaskType(IType taskType)
        {
            if (CardControl != null)
                CardControl.TaskCardViewModel.IsValidInputChanged -= OnIsValidInputChanged;

            _taskType = taskType;
            NotifyOfPropertyChange(nameof(TaskType));
            CardControl = new WorkflowTaskCardControlViewModel(taskType, _pilotServices, _invalidateData);
            CardControl.TaskCardViewModel.IsValidInputChanged += OnIsValidInputChanged;

            _invalidateData.Invalidate();
        }
    }
}