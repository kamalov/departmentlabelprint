﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ascon.Pilot.SDK.Controls;
using Ascon.Pilot.SDK.Controls.Commands;
using Ascon.Pilot.SDK.Controls.ObjectCardView;
using Ascon.Pilot.SDK.TaskSample.Services;

namespace Ascon.Pilot.SDK.TaskSample.Workflow
{
    class StageViewModel : PropertyChangedBase, IOrgUnitsListManager, IInvalidateData
    {
        private readonly IPilotServices _pilotServices;
        private readonly ITaskTypesService _taskTypesService;
        private readonly DelegateCommand _showExecutorsListCommand;
        private readonly DelegateCommand<int?> _removeOrgUnitItemCommand;
        private string _caption;
        private WorkflowExecutor _selectedExecutor;

        public StageViewModel(int index, IType stageType, IStageManager stageManager, IPilotServices pilotServices, ITaskTypesService taskTypesService)
        {
            Type = stageType;
            _pilotServices = pilotServices;
            _taskTypesService = taskTypesService;
            StageManager = stageManager;
            Index = index;
            Caption = "Add executors";
            
            _showExecutorsListCommand = new DelegateCommand(ShowExecutorsList);
            _removeOrgUnitItemCommand = new DelegateCommand<int?>(RemoveExecutor);
        }

        public bool IsReadonly => false;
        public ObservableCollection<WorkflowExecutor> Executors { get; } = new ObservableCollection<WorkflowExecutor>();
        public IStageManager StageManager { get; }
        public IRaiseCanExecuteChangedCommand ShowOrgUnitItemsListCommand => _showExecutorsListCommand;
        public IRaiseCanExecuteChangedCommand RemoveOrgUnitItemCommand => _removeOrgUnitItemCommand;
        public int Index { get; }
        public bool IsDeleteButtonVisible => Index != 0;

        public string Caption
        {
            get => _caption;
            set
            {
                _caption = value;
                NotifyOfPropertyChange(nameof(Caption));
            }
        }

        public WorkflowExecutor SelectedExecutor
        {
            get => _selectedExecutor;
            set
            {
                _selectedExecutor = value;
                NotifyOfPropertyChange(nameof(SelectedExecutor));
                StageManager.Selected(_selectedExecutor);
            }
        }

        public IType Type { get; }

        private void ShowExecutorsList()
        {
            var dialogOptions = _pilotServices.PilotDialogService.NewOptions()
                .WithParentWindow(Win32Service.GetActiveWindow())
                .WithCaption("Select executors")
                .WithOkButtonCaption("Select")
                .WithAllowMultiSelect(true)
                .WithAllowChecking(true);

            var executors = _pilotServices.PilotDialogService.ShowOrganisationUnitSelectorDialog(dialogOptions).Where(x => x.Children.Count == 0).ToList();
            if (!executors.Any())
                return;

            Executors.Clear();
            var types = _taskTypesService.GetStageTaskTypes(Type).ToList();
            var type = types.FirstOrDefault();
            foreach (var executor in executors)
            {
                Executors.Add(new WorkflowExecutor(type, _pilotServices, this, executor.Id, types, this));
            }

            if (Executors.Any())
            {
                Caption = $"Stage {Index}";
            }

            Invalidate();
        }

        private void RemoveExecutor(int? item)
        {
            var wfItem = Executors.FirstOrDefault(w => item != null && w.PositionId == item.Value);
            Executors.Remove(wfItem);
            Invalidate();
        }

        public void Invalidate()
        {
            StageManager.Invalidate();
            StageManager.Selected(SelectedExecutor);
        }
    }
}
