﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Ascon.Pilot.SDK.Controls;
using Ascon.Pilot.SDK.Controls.Commands;
using Ascon.Pilot.SDK.Controls.ObjectCardView;
using Ascon.Pilot.SDK.TaskSample.Extensions;
using Ascon.Pilot.SDK.TaskSample.Services;
using Ascon.Pilot.SDK.TaskSample.TaskEditView.TaskObjectCard;

namespace Ascon.Pilot.SDK.TaskSample.Workflow
{
    class WorkflowViewModel : PropertyChangedBase, IStageManager, IInvalidateData
    {
        private readonly IPilotServices _pilotServices;
        private readonly IObjectModifier _modifier;
        private readonly ITaskTypesService _taskTypesService;
        private readonly DelegateCommand _submitCommand;
        private readonly DelegateCommand<int?> _deleteStageCommand;
        private readonly DelegateCommand _addStageCommand;
        private TaskCardControlViewModel _taskCardControlViewModel;

        public WorkflowViewModel(IType type, IPilotServices pilotServices, ITaskTypesService taskTypesService)
        {
            _pilotServices = pilotServices;
            _taskTypesService = taskTypesService;
            _modifier = pilotServices.ObjectModifier;

            Type = type;

            _submitCommand = new DelegateCommand(SubmitWorkflow, CanSubmit);
            _deleteStageCommand = new DelegateCommand<int?>(DeleteStage, CanDeleteStage);
            _addStageCommand = new DelegateCommand(CreateStage);

            WorkflowCardViewModel = new ObjectCardViewModel(pilotServices.ObjectsRepository, pilotServices.PilotDialogService, pilotServices.AttributeFormatParser);
            WorkflowCardViewModel.IsValidInputChanged += OnIsValidInputChanged;
            WorkflowCardViewModel.CreateCard(type);

            CreateStage();
        }

        public IType Type { get; }
        public ObjectCardViewModel WorkflowCardViewModel { get; }

        public TaskCardControlViewModel TaskCardControlViewModel
        {
            get => _taskCardControlViewModel;
            private set
            {
                _taskCardControlViewModel = value;
                NotifyOfPropertyChange(nameof(TaskCardControlViewModel));
            }
        }
        public ObservableCollection<StageViewModel> Stages { get; } = new ObservableCollection<StageViewModel>();
        public ICommand DeleteStageCommand => _deleteStageCommand;
        public ICommand AddStageCommand => _addStageCommand;
        public ICommand SubmitCommand => _submitCommand;

        private void OnIsValidInputChanged(object sender, EventArgs e)
        {
            Invalidate();
        }

        public void Invalidate()
        {
            _submitCommand.RaiseCanExecuteChanged();
        }

        public void Selected(WorkflowExecutor executor)
        {
            if (executor == null)
            {
                TaskCardControlViewModel = null;
                return;
            }

            TaskCardControlViewModel = executor.CardControl;
        }

        private void SubmitWorkflow()
        {
            var person = _pilotServices.ObjectsRepository.GetCurrentPerson();

            // create workflow
            var workflowBuilder = _modifier.CreateWorkflow(Type, Guid.Empty); //Create in tasks root
            foreach (var pair in WorkflowCardViewModel.Values)
            {
                var attributeName = pair.Key;
                var value = pair.Value.SetToUniversalTimeIfNeeded();
                workflowBuilder.SetAttributeValue(attributeName, value);
            }

            workflowBuilder.SetInitiator(person.MainPosition.Position);

            // create stages and tasks
            foreach (var stage in Stages)
            {
                var stageBuilder = _modifier.CreateStage(stage.Index, workflowBuilder.DataObject.Id, stage.Type)
                    .SetInitiator(person.MainPosition.Position);

                foreach (var executorItem in stage.Executors)
                {
                    var taskBuilder = _modifier.CreateTask(executorItem.TaskType, stageBuilder.DataObject.Id)
                        .SetExecutor(executorItem.PositionId)
                        .SetInitiator(person.MainPosition.Position)
                        .SetAutocompleteParent(true);

                    // fill task for stage
                    foreach (var valuePair in executorItem.CardControl.TaskCardViewModel.Values)
                    {
                        var value = valuePair.Value.SetToUniversalTimeIfNeeded();
                        taskBuilder.SetAttributeValue(valuePair.Key, value);
                    }

                    taskBuilder.SetAttachments(executorItem.CardControl.AttachmentsViewModel.Attachments.Select(x => x.Id).ToList(), _modifier);
                    var collection = executorItem.CardControl.TaskCardViewModel.ViewModelCollection;
                    var deadline = collection.FirstOrDefault(x => x.Attribute.Name == SystemTaskAttributes.DEADLINE_DATE)?.Value as DateTime?;
                    taskBuilder.SetDeadline(deadline, false);
                }
            }
            
            _modifier.Apply();
            CloseView(true);
        }

        private bool CanSubmit()
        {
            var allStagesNotEmpty = Stages.All(s => s.Executors.Any());
            var allObligatoryAttrsIsSet = Stages.All(s => s.Executors.All(e => e.CardControl.TaskCardViewModel.IsValidInput));
            return WorkflowCardViewModel.IsValidInput && allStagesNotEmpty && allObligatoryAttrsIsSet;
        }

        private bool CanDeleteStage(int? index)
        {
            return index != 0;
        }

        private void DeleteStage(int? index)
        {
            var stage = Stages.FirstOrDefault(s => s.Index == index);
            Stages.Remove(stage);

            Invalidate();
        }

        private void CreateStage()
        {
            var stageType = _taskTypesService.GetStageType(Type);
            var stage = new StageViewModel(Stages.Count + 1, stageType, this, _pilotServices, _taskTypesService);
            Stages.Add(stage);

            Invalidate();
        }
    }
}
