﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ascon.Pilot.SDK.Controls.ObjectCardView;
using Ascon.Pilot.SDK.TaskSample.Services;
using Ascon.Pilot.SDK.TaskSample.TaskEditView.TaskObjectCard;

namespace Ascon.Pilot.SDK.TaskSample.Workflow
{
    public sealed class WorkflowTaskCardControlViewModel : TaskCardControlViewModel
    {
        public WorkflowTaskCardControlViewModel(IType type, IPilotServices pilotServices, IInvalidateData invalidateData) 
            : base(type, pilotServices, invalidateData)
        {
        }

        protected override ObjectCardViewModel CreateObjectCardViewModel(IObjectsRepository repository, IPilotDialogService pilotDialogService, IAttributeFormatParser attributeFormatParser)
        {
            return new WorkflowTaskObjectCardViewModel(repository, pilotDialogService, attributeFormatParser);
        }
    }

    sealed class WorkflowTaskObjectCardViewModel : ObjectCardViewModel
    {
        public WorkflowTaskObjectCardViewModel(IObjectsRepository repository, IPilotDialogService dialogService,
            IAttributeFormatParser attributeFormatParser)
            : base(repository, dialogService, attributeFormatParser)
        {
        }

        public override CardControlViewModel CreateCardControlViewModel(IAttribute attribute, IType type, object initValue, bool isEditMode, bool isReadOnlyAttribute)
        {
            switch (attribute.Type)
            {
                case AttributeType.OrgUnit:
                    if (attribute.Name == SystemTaskAttributes.EXECUTOR_POSITION)
                        return null;

                    return new OrgUnitCardControlViewModel(attribute, type, initValue, isReadOnlyAttribute, _dialogService, _repository, this, _attributeFormatParser, isEditMode);

                default:
                    return new CardControlViewModel(attribute, type, initValue, isReadOnlyAttribute, _dialogService, _repository, this, _attributeFormatParser, isEditMode);
            }
        }
    }
}
