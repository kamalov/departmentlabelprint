﻿using System.Windows.Input;

namespace Ascon.Pilot.SDK.TaskSample.Workflow
{
    public interface IStageManager
    {
        ICommand DeleteStageCommand { get; }
        void Invalidate();
        void Selected(WorkflowExecutor executor);
    }
}