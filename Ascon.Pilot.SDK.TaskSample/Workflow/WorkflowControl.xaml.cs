﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Ascon.Pilot.SDK.TaskSample.Workflow
{
    /// <summary>
    /// Interaction logic for WorkflowControl.xaml
    /// </summary>
    public partial class WorkflowControl : UserControl
    {
        public WorkflowControl()
        {
            InitializeComponent();
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            if (Parent is Window window)
                window.Close();
        }
    }
}
