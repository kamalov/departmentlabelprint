namespace Ascon.Pilot.SDK.TaskSample.Workflow
{
    public class TaskTypeItem
    {
        public TaskTypeItem(IType type)
        {
            Type = type;
        }

        public IType Type { get; }

        public string Title => Type.Title;

        public string Name => Type.Name;

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            return obj is TaskTypeItem other && other.Type.Id == Type.Id;
        }

        public override int GetHashCode()
        {
            return Type.Id.GetHashCode();
        }
    }
}