﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ascon.Pilot.SDK.TaskSample.Services
{
    public interface IPilotServices
    {
        IObjectModifier ObjectModifier { get; }
        IObjectsRepository ObjectsRepository { get; }
        IPilotDialogService PilotDialogService { get; }
        ITabServiceProvider TabServiceProvider { get; }
        IAttributeFormatParser AttributeFormatParser { get; }
    }

    [Export(typeof(IPilotServices))]
    class PilotServices : IPilotServices
    {
        [ImportingConstructor]
        public PilotServices(IObjectModifier objectModifier, IObjectsRepository objectsRepository, 
            IPilotDialogService pilotDialogService, ITabServiceProvider tabServiceProvider, IAttributeFormatParser attributeFormatParser)
        {
            ObjectModifier = objectModifier;
            ObjectsRepository = objectsRepository;
            PilotDialogService = pilotDialogService;
            TabServiceProvider = tabServiceProvider;
            AttributeFormatParser = attributeFormatParser;
        }
        public IObjectModifier ObjectModifier { get; }
        public IObjectsRepository ObjectsRepository { get; }
        public IPilotDialogService PilotDialogService { get; }
        public ITabServiceProvider TabServiceProvider { get; }
        public IAttributeFormatParser AttributeFormatParser { get; }
    }
}
