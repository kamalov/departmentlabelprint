﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Ascon.Pilot.SDK.TaskSample.Extensions;
using Ascon.Pilot.SDK.TaskSample.TaskEditView;
using Ascon.Pilot.SDK.TaskSample.Workflow;
using Ascon.Pilot.Theme.Controls;

namespace Ascon.Pilot.SDK.TaskSample.Services
{
    public interface IDialogService
    {
        void ShowDialog(string title, FrameworkElement view, object viewModel);
        void ShowNewTaskDialog(IType type);
        void ShowNewWorkflowDialog(IType type, ITaskTypesService taskTypesService);
        void ShowWorkflowDialogWithData(ITaskTypesService taskTypesService);
        void ShowTaskDialogWithData(ITaskTypesService taskTypesService);
    }

    [Export(typeof(IDialogService))]
    class DialogService : IDialogService
    {
        private readonly IPilotServices _pilotServices;
        private readonly IPilotDialogService _pilotDialogService;
        private readonly IObjectModifier _objectModifier;
        private readonly IObjectsRepository _repository;

        [ImportingConstructor]
        public DialogService(IPilotServices pilotServices)
        {
            _pilotServices = pilotServices;
            _pilotDialogService = pilotServices.PilotDialogService;
            _objectModifier = pilotServices.ObjectModifier;
            _repository = pilotServices.ObjectsRepository;
        }

        public void ShowDialog(string title, FrameworkElement view, object viewModel)
        {
            var window = new PureWindow
            {
                Title = title,
                Content = view,
                DataContext = viewModel,
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
                Height = 600,
                Width = 800
            };
            window.Show();
        }

        public void ShowNewTaskDialog(IType type)
        {
            var taskEditViewModel = new TaskEditViewModel(type, _pilotServices);
            var taskEditView = new TaskEditView.TaskEditView { DataContext = taskEditViewModel };
            ShowDialog("New task - " + type.Title, taskEditView, taskEditViewModel);
        }

        public void ShowNewWorkflowDialog(IType type, ITaskTypesService taskTypesService)
        {
            var viewModel = new WorkflowViewModel(type, _pilotServices, taskTypesService);
            var view = new WorkflowControl { DataContext = viewModel };
            ShowDialog("New workflow - " + type.Title, view, viewModel);
        }

        public void ShowWorkflowDialogWithData(ITaskTypesService taskTypesService)
        {
            var modifier = _objectModifier;
            var person = _repository.GetCurrentPerson();
            var initiatorPositionId = person.MainPosition.Position;

            // create workflow
            var workflowType = taskTypesService.RootWorkflowTypes.First();
            var workflowBuilder = modifier.CreateWorkflow(workflowType, Guid.Empty); //Create in tasks root
            // fill workflow attributes (only string type for example)
            foreach (var attribute in workflowType.Attributes)
            {
                if (attribute.Type == AttributeType.String)
                    workflowBuilder.SetAttributeValue(attribute.Name, "Test string");
            }
            // set initiator to workflow
            workflowBuilder.SetInitiator(initiatorPositionId);

            // create stage
            var stageType = taskTypesService.GetStageType(workflowType);
            var stageBuilder = modifier.CreateStage(1, workflowBuilder.DataObject.Id, stageType)
                .SetInitiator(initiatorPositionId); //set initiator to stage

            // Add task to stage
            var executorPosition = _repository.GetOrganisationUnits().Last(o => o.IsPosition);
            var taskType = taskTypesService.GetStageTaskTypes(stageType).First();
            var taskBuilder = modifier.CreateTask(taskType, stageBuilder.DataObject.Id)
                .SetExecutor(executorPosition.Id)
                .SetInitiator(initiatorPositionId) //set initiator to task
                .SetAutocompleteParent(true);

            // fill task attributes (only string type for example)
            foreach (var attribute in taskType.Attributes)
            {
                if (attribute.Type == AttributeType.String)
                    taskBuilder.SetAttributeValue(attribute.Name, "Test string");
            }

            _pilotDialogService.ShowWorkflowDialog(workflowType.Id, modifier);
        }

        public void ShowTaskDialogWithData(ITaskTypesService taskTypesService)
        {
            var modifier = _objectModifier;
            var person = _repository.GetCurrentPerson();
            var initiatorPositionId = person.MainPosition.Position;

            // Add task to stage
            var executorPosition = _repository.GetOrganisationUnits().Last(o => o.IsPosition);
            var taskType = taskTypesService.RootTaskTypes.First();
            var taskBuilder = modifier.CreateTask(taskType, Guid.Empty) //create in root
                .SetExecutor(executorPosition.Id)
                .SetInitiator(initiatorPositionId) //set initiator to task
                .SetDeadline(DateTime.UtcNow.AddDays(10), false)
                .SetAutocompleteParent(true);

            // fill task attributes (only string type for example)
            foreach (var attribute in taskType.Attributes)
            {
                if (attribute.Type == AttributeType.String)
                    taskBuilder.SetAttributeValue(attribute.Name, "Test string");
            }

            _pilotDialogService.ShowTaskDialog(taskType.Id, modifier);
        }
    }
}
