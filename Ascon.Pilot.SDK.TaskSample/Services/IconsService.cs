﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ascon.Pilot.SDK.TaskSample.Services
{
    class IconsService
    {
        public static byte[] CreateTaskIcon => GetIcon("CreateTask.svg");
        public static byte[] CreateWorkflowIcon => GetIcon("CreateWorkflow.svg");

        private static byte[] GetIcon(string resourceName)
        {
            using (var stream = typeof(IconsService).Assembly.GetManifestResourceStream($"Ascon.Pilot.SDK.TaskSample.Icons.{resourceName}"))
            {
                return ReadBytes(stream);
            }
        }

        private static byte[] ReadBytes(Stream input)
        {
            using (var ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
    }
}
