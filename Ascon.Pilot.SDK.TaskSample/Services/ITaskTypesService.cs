﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Ascon.Pilot.SDK.TaskSample.Extensions;

namespace Ascon.Pilot.SDK.TaskSample.Services
{
    public interface ITaskTypesService
    {
        IType RootType { get; }
        List<IType> RootTaskTypes { get; }
        List<IType> RootWorkflowTypes { get; }
        List<IType> AllTaskTypes { get; }
        List<IType> AllWorkflowTypes { get; }

        IType GetStageType(IType workflowType);
        IEnumerable<IType> GetStageTaskTypes(IType stageType);
        IEnumerable<IType> GetSubTaskTypes(IType type);
        IEnumerable<IType> GetSubWorkflowTypes(IType type);
    }

    [Export(typeof(ITaskTypesService))]
    public class TaskTypesService : ITaskTypesService
    {
        private readonly IDictionary<int, IType> _types;

        [ImportingConstructor]
        public TaskTypesService(IObjectsRepository repository)
        {
            _types = repository.GetTypes().ToDictionary(k => k.Id, v => v);
            RootType = _types.Values.FirstOrDefault(x => x.Name.Equals(SystemTypeNames.TASKS_ROOT_TYPE_NAME, StringComparison.OrdinalIgnoreCase));
            var rootTypes = RootType != null ? GetTypes(RootType.Children).ToList() : new List<IType>();
            RootTaskTypes = rootTypes.Where(t => t.IsTaskType()).ToList();
            RootWorkflowTypes = rootTypes.Where(t => t.IsWorkflowType()).ToList();

            var taskSet = new HashSet<IType>();
            var workflowSet = new HashSet<IType>();

            if (RootType != null)
                GetAllTypes(RootType, taskSet, workflowSet);

            AllTaskTypes = taskSet.ToList();
            AllWorkflowTypes = workflowSet.ToList();
        }

        public IType RootType { get; }
        public List<IType> RootTaskTypes { get; }
        public List<IType> RootWorkflowTypes { get; }
        public List<IType> AllTaskTypes { get; }
        public List<IType> AllWorkflowTypes { get; }

        public IType GetStageType(IType workflowType)
        {
            var childrenTypes = GetTypes(workflowType.Children).ToList();
            var stageType = childrenTypes.FirstOrDefault(t => t.IsWorkflowStageType());
            return stageType;
        }

        public IEnumerable<IType> GetStageTaskTypes(IType stageType)
        {
            return GetTaskTypes(stageType);
        }

        public IEnumerable<IType> GetSubTaskTypes(IType type)
        {
            return GetTaskTypes(type);
        }

        public IEnumerable<IType> GetSubWorkflowTypes(IType type)
        {
            var childrenTypes = GetTypes(type.Children).ToList();
            var workflowTypes = childrenTypes.Where(t => t.IsWorkflowType());
            return workflowTypes;
        }

        private IEnumerable<IType> GetTaskTypes(IType type)
        {
            if (type == null)
                return Enumerable.Empty<IType>();

            var childrenTypes = GetTypes(type.Children).ToList();
            var taskTypes = childrenTypes.Where(t => t.IsTaskType());
            return taskTypes;
        }

        private void GetAllTypes(IType type, HashSet<IType> taskSet, HashSet<IType> workflowSet)
        {
            var childrenTypes = GetTypes(type.Children).ToList();
            foreach (var childrenType in childrenTypes)
            {
                if (taskSet.Contains(childrenType) || workflowSet.Contains(childrenType))
                    continue;

                if (childrenType.IsWorkflowType())
                    workflowSet.Add(childrenType);

                if (childrenType.IsTaskType())
                    taskSet.Add(childrenType);

                GetAllTypes(childrenType, taskSet, workflowSet);
            }
        }

        private IEnumerable<IType> GetTypes(IEnumerable<int> ids)
        {
            return ids.Select(x => _types[x]);
        }
    }
}
