﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ascon.Pilot.SDK.TaskSample.Services
{
    public interface IInvalidateData
    {
        void Invalidate();
    }
}
