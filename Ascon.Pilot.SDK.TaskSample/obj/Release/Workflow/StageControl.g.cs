﻿#pragma checksum "..\..\..\Workflow\StageControl.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "EF8F6F467D86F0F506940A4E38320E5B533209D9"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Ascon.Pilot.SDK.Controls.Converters;
using Ascon.Pilot.SDK.Controls.ObjectCardView;
using Ascon.Pilot.SDK.TaskSample;
using Ascon.Pilot.SDK.TaskSample.TaskEditView.Tools;
using Ascon.Pilot.SDK.TaskSample.Workflow;
using Ascon.Pilot.Theme;
using Ascon.Pilot.Theme.Controls;
using Ascon.Pilot.Theme.Tools;
using Ascon.Pilot.Theme.Tools.Converters;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Ascon.Pilot.SDK.TaskSample.Workflow {
    
    
    /// <summary>
    /// StageControl
    /// </summary>
    public partial class StageControl : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 191 "..\..\..\Workflow\StageControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid stageItemLayoutRoot;
        
        #line default
        #line hidden
        
        
        #line 206 "..\..\..\Workflow\StageControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock stageCaption;
        
        #line default
        #line hidden
        
        
        #line 211 "..\..\..\Workflow\StageControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Documents.Run stageCaptionRun;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\..\Workflow\StageControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock deleteStageTbl;
        
        #line default
        #line hidden
        
        
        #line 221 "..\..\..\Workflow\StageControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Documents.Hyperlink deleteStageLnk;
        
        #line default
        #line hidden
        
        
        #line 231 "..\..\..\Workflow\StageControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView executorsItemsControl;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Ascon.Pilot.SDK.TaskSample.ext2;component/workflow/stagecontrol.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Workflow\StageControl.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.stageItemLayoutRoot = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.stageCaption = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.stageCaptionRun = ((System.Windows.Documents.Run)(target));
            return;
            case 4:
            this.deleteStageTbl = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.deleteStageLnk = ((System.Windows.Documents.Hyperlink)(target));
            return;
            case 6:
            this.executorsItemsControl = ((System.Windows.Controls.ListView)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

