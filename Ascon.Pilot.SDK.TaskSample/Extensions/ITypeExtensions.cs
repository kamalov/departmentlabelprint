﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ascon.Pilot.SDK.TaskSample.Extensions
{
    static class TypeExtensions
    {
        public static bool IsTaskType(this IType type)
        {
            return type.Name.StartsWith(SystemTypeNames.TASK_PREFIX);
        }

        public static bool IsWorkflowType(this IType type)
        {
            return type.Name.StartsWith(SystemTypeNames.WORKFLOW_PREFIX);
        }

        public static bool IsWorkflowStageType(this IType type)
        {
            return type.Name.StartsWith(SystemTypeNames.STAGE_PREFIX);
        }
    }
}
