﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ascon.Pilot.SDK.TaskSample.Extensions
{
    static class ModifierExtensions
    {
        public static IObjectBuilder CreateTask(this IObjectModifier modifier, IType type)
        {
            var builder = modifier.Create(Guid.Empty, type) // Guid.Empty create task in root
                .SetAttribute(SystemTaskAttributes.STATE, SystemStates.TASK_NONE_STATE_ID)
                .SetAccessRights(0, AccessLevel.ViewEditAgrement, DateTime.MaxValue, true);

            return builder;
        }

        public static IObjectBuilder CreateTask(this IObjectModifier modifier, IType type, Guid parentId)
        {
            var builder = modifier.Create(parentId, type)
                .SetAttribute(SystemTaskAttributes.STATE, SystemStates.TASK_NONE_STATE_ID)
                .SetAccessRights(0, AccessLevel.ViewEditAgrement, DateTime.MaxValue, true);

            return builder;
        }

        public static IObjectBuilder CreateWorkflow(this IObjectModifier modifier, IType type, Guid parentId)
        {
            if (type == null || type.Id == -1)
                throw new Exception("Workflow type not found in the configuration.");

            var builder = modifier.Create(parentId, type)
                .SetAttribute(SystemTaskAttributes.STATE, SystemStates.TASK_NONE_STATE_ID)
                .SetAccessRights(0, AccessLevel.ViewEditAgrement, DateTime.MaxValue, true);

            return builder;
        }

        public static IObjectBuilder CreateStage(this IObjectModifier modifier, int order, Guid parentId, IType stageType)
        {
            if (stageType == null || stageType.Id == -1)
                throw new Exception("Workflow stage type not found in the configuration.");

            var builder = modifier.Create(parentId, stageType)
                .SetAttribute(SystemTaskAttributes.STAGE_ORDER, order)
                .SetAttribute(SystemTaskAttributes.STATE, SystemStates.TASK_NONE_STATE_ID)
                .SetAutocompleteParent(true);

            return builder;
        }
    }
}
