﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ascon.Pilot.SDK.TaskSample.Extensions
{
    static class StringExtensions
    {
        public static string GetXamlName(this string value)
        {
            return new string(value.Where(char.IsLetterOrDigit).ToArray());
        }
    }
}
