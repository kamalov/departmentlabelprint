﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ascon.Pilot.SDK.Controls.ObjectCardView;

namespace Ascon.Pilot.SDK.TaskSample.Extensions
{
    static class ObjectChangeBuilderExtensions
    {
        public static IObjectBuilder SetDeadline(this IObjectBuilder builder, DateTime? deadlineDate, bool isRelative)
        {
            var newDeadline = deadlineDate ?? DateTime.MaxValue;
            if (newDeadline != DateTime.MaxValue)
                newDeadline = newDeadline.ToUniversalTime();

            if (isRelative && newDeadline != DateTime.MaxValue)
            {
                var localDeadline = newDeadline.ToLocalTime().Date.Add(new TimeSpan(23, 59, 59));
                var split = localDeadline - DateTime.Today;

                var timeZoneSplit = DateTime.Today - DateTime.Today.ToUniversalTime();
                split = split.Add(timeZoneSplit.Negate());

                builder.SetAttribute(SystemTaskAttributes.RELATIVE_DEADLINE, DeadlineSplit.ToString(split, timeZoneSplit));
                builder.SetAttribute(SystemTaskAttributes.DEADLINE_DATE, DateTime.MaxValue);
                return builder;
            }

            builder.RemoveAttribute(SystemTaskAttributes.RELATIVE_DEADLINE);
            builder.SetAttribute(SystemTaskAttributes.DEADLINE_DATE, newDeadline);
            return builder;
        }

        public static IObjectBuilder SetInitiator(this IObjectBuilder builder, int positionId)
        {
            builder.SetAttribute(SystemTaskAttributes.INITIATOR_POSITION, new[] { positionId });
            return builder;
        }

        public static IObjectBuilder SetExecutor(this IObjectBuilder builder, int positionId)
        {
            builder.SetAttribute(SystemTaskAttributes.EXECUTOR_POSITION, new[] { positionId });
            return builder;
        }

        public static IObjectBuilder SetAutocompleteParent(this IObjectBuilder builder, bool value)
        {
            builder.SetAttribute(SystemTaskAttributes.AUTO_COMPLETE_PARENT, value.ToInt());
            return builder;
        }

        public static IObjectBuilder SetAttributeValue(this IObjectBuilder builder, string key, DValue value)
        {
            if (value.StrValue != null)
                builder.SetAttribute(key, value.StrValue);
            if (value.IntValue != null)
                builder.SetAttribute(key, value.IntValue.Value);
            if (value.DoubleValue != null)
                builder.SetAttribute(key, value.DoubleValue.Value);
            if (value.DateValue != null)
                builder.SetAttribute(key, value.DateValue.Value);
            if (value.DecimalValue != null)
                builder.SetAttribute(key, value.DecimalValue.Value);
            if (value.GuidValue != null)
                builder.SetAttribute(key, value.GuidValue.Value);
            if (value.ArrayIntValue != null)
                builder.SetAttribute(key, value.ArrayIntValue);
            
            return builder;
        }

        public static IObjectBuilder SetAttachments(this IObjectBuilder builder, IEnumerable<Guid> attachments, IObjectModifier modifier)
        {
            if (attachments == null)
                return builder;

            var source = builder.DataObject;
            var currentAttachments = source.Relations.Where(x => x.Type == ObjectRelationType.TaskAttachments).ToList();
            var attachmentsToApply = attachments.ToList();

            var relationsToRemove = currentAttachments.Where(x => attachmentsToApply.All(a => a != x.TargetId)).ToList();
            var relationsToAdd = attachmentsToApply.Where(x => currentAttachments.All(a => a.TargetId != x)).Select(
                x =>
                {
                    var attachmentId = x;
                    return new Relation
                    {
                        Id = Guid.NewGuid(),
                        TargetId = attachmentId,
                        Type = ObjectRelationType.TaskAttachments
                    };
                }
            ).ToList();

            foreach (var relation in relationsToAdd)
            {
                modifier.Link(source.Id, relation);
            }

            foreach (var relation in relationsToRemove)
            {
                modifier.RemoveLink(source, relation);
            }
            return builder;
        }

        private static void Link(this IObjectModifier modifier, Guid taskId, IRelation relation)
        {
            var backRelation = new Relation
            {
                Id = relation.Id,
                TargetId = taskId,
                Type = relation.Type,
                Name = relation.Name
            };
            
            modifier.CreateLink(relation, backRelation);
        }
    }

    public class DeadlineSplit
    {
        private DeadlineSplit(TimeSpan split, TimeSpan timeZoneSplit)
        {
            Split = split;
            TimeZoneSplit = timeZoneSplit;
        }

        public TimeSpan Split { get; }
        public TimeSpan TimeZoneSplit { get; }

        public static string ToString(TimeSpan split, TimeSpan timeZoneSplit)
        {
            return $"{split}*{timeZoneSplit}";
        }

        public static DeadlineSplit Parse(string splitStr)
        {
            if (string.IsNullOrEmpty(splitStr))
                return null;

            var parts = splitStr.Split('*');
            if (parts.Length != 2)
                return null;

            TimeSpan split;
            if (!TimeSpan.TryParse(parts[0], out split))
                return null;

            TimeSpan timeZoneSplit;
            if (!TimeSpan.TryParse(parts[1], out timeZoneSplit))
                return null;

            return new DeadlineSplit(split, timeZoneSplit);
        }
    }

    public static class DeadlineSplitCalculator
    {
        public static DateTime GetNewDeadlineFromNow(this DeadlineSplit deadlineSplit)
        {
            return GetNewDeadlineFrom(deadlineSplit, DateTime.UtcNow);
        }

        public static DateTime GetNewDeadlineFrom(this DeadlineSplit deadlineSplit, DateTime utcNow)
        {
            utcNow = utcNow.ToUniversalTime();
            var utcToday = utcNow.Date;

            var taskOwnerNow = utcNow.Add(deadlineSplit.TimeZoneSplit);
            var timeZoneNowDaysSplit = taskOwnerNow.Date - utcNow.Date;

            var newDeadline = utcToday.Add(deadlineSplit.Split).AddDays(timeZoneNowDaysSplit.Days);

            return newDeadline;
        }
    }
}
