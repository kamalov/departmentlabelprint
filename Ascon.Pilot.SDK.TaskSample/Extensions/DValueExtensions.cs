﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ascon.Pilot.SDK.Controls.ObjectCardView;

namespace Ascon.Pilot.SDK.TaskSample.Extensions
{
    static class DValueExtensions
    {
        public static int ToInt(this bool value)
        {
            return value ? 1 : 0;
        }

        public static bool ToBool(this int value)
        {
            return value == 1 ? true : false;
        }

        public static DValue SetToUniversalTimeIfNeeded(this DValue value)
        {
            if (value.DateValue.HasValue)
            {
                return value.DateValue.Value.ToUniversalTime();
            }

            return value;
        }
    }
}
