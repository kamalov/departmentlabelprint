﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ascon.Pilot.SDK.TaskSample
{
    internal static class RepositoryExtensions
    {
        public static  IPerson GetPersonOnOrganizationUnit(this IObjectsRepository repository, int id)
        {
            var people = repository.GetPeople();
            return people.Where(p => p.Positions.Select(m => m.Position).Contains(id))
                                                .OrderBy(o => o.Positions.First(x => x.Position == id).Order)
                                                .FirstOrDefault();
        }
    }
}
