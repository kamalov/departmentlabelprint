﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Ascon.Pilot.SDK.TaskSample.Extensions;
using Ascon.Pilot.SDK.TaskSample.Services;
using Ascon.Pilot.SDK.Toolbar;
using Ascon.Pilot.Theme.ColorScheme;

namespace Ascon.Pilot.SDK.TaskSample
{
    [Export(typeof(IToolbar<TasksViewContext2>))]
    public class TaskToolbarSample : IToolbar<TasksViewContext2>
    {
        private const string CREATE_NEW_TASK = "tbiCreateNewTask";
        private const string CREATE_NEW_WORKFLOW = "tbiCreateNewWorkflow";
        private const string SHOW_WORKFLOW_DIALOG_WITH_PREDEFINED_DATA = "miShowWorkflowDialogWithPredefinedData";
        private const string SHOW_TASK_DIALOG_WITH_PREDEFINED_DATA = "miShowTaskDialogWithPredefinedData";

        private readonly Dictionary<string, IType> _types = new Dictionary<string, IType>();
        private readonly ITaskTypesService _taskTypesService;
        private readonly IDialogService _dialogService;

        [ImportingConstructor]
        public TaskToolbarSample(IPilotServices pilotServices, IDialogService dialogService, ITaskTypesService taskTypesService)
        {
            _dialogService = dialogService;
            _taskTypesService = taskTypesService;

            var accentColor = (Color)ColorConverter.ConvertFromString(pilotServices.PilotDialogService.AccentColor);
            ColorScheme.Initialize(accentColor, pilotServices.PilotDialogService.Theme);
        }

        public void Build(IToolbarBuilder builder, TasksViewContext2 context)
        {
            builder.AddSeparator(2);

            var taskTypes = _taskTypesService.RootTaskTypes;
            var subMenuHandler = new ToolbarItemSubmenuHandler(taskTypes, _types);
            builder.AddMenuButtonItem(CREATE_NEW_TASK, 3)
                .WithMenu(subMenuHandler)
                .WithHeader("Create new task")
                .WithIcon(IconsService.CreateTaskIcon);

            var workflowTypes = _taskTypesService.RootWorkflowTypes;
            var subMenuWorkflowHandler = new ToolbarItemSubmenuHandler(workflowTypes, _types);
            builder.AddMenuButtonItem(CREATE_NEW_WORKFLOW, 4)
                .WithMenu(subMenuWorkflowHandler)
                .WithHeader("Create new workflow")
                .WithIcon(IconsService.CreateWorkflowIcon);

            builder.AddButtonItem(SHOW_TASK_DIALOG_WITH_PREDEFINED_DATA, 5)
                .WithHeader("Show task dialog with predefined data")
                .WithIcon(IconsService.CreateTaskIcon);

            builder.AddButtonItem(SHOW_WORKFLOW_DIALOG_WITH_PREDEFINED_DATA, 6)
                .WithHeader("Show workflow dialog with predefined data")
                .WithIcon(IconsService.CreateWorkflowIcon);

            if (builder.ItemNames.Count() > 6)
                builder.AddSeparator(7);
        }

        public void OnToolbarItemClick(string name, TasksViewContext2 context)
        {
            if (name == SHOW_WORKFLOW_DIALOG_WITH_PREDEFINED_DATA)
            {
                _dialogService.ShowWorkflowDialogWithData(_taskTypesService);
                return;
            }

            if (name == SHOW_TASK_DIALOG_WITH_PREDEFINED_DATA)
            {
                _dialogService.ShowTaskDialogWithData(_taskTypesService);
                return;
            }

            if (!_types.TryGetValue(name, out var type))
                return;

            if (type.IsWorkflowType())
            {
                _dialogService.ShowNewWorkflowDialog(type, _taskTypesService);
                return;
            }

            if (type.IsTaskType())
            {
                _dialogService.ShowNewTaskDialog(type);
            }
        }
    }

    class ToolbarItemSubmenuHandler : IToolbarItemSubmenuHandler
    {
        private readonly List<IType> _taskTypes;
        private readonly Dictionary<string, IType> _typesFoFill;

        public ToolbarItemSubmenuHandler(List<IType> taskTypes, Dictionary<string, IType> typesFoFill)
        {
            _taskTypes = taskTypes;
            _typesFoFill = typesFoFill;
        }

        public void OnSubmenuRequested(IToolbarBuilder builder)
        {
            for (var i = 0; i < _taskTypes.Count; i++)
            {
                var type = _taskTypes[i];
                var typeName = type.Name.GetXamlName();
                builder.AddButtonItem(typeName, i).WithHeader(type.Title).WithIcon(type.SvgIcon);
                _typesFoFill[typeName] = type;
            }
        }
    }
}
