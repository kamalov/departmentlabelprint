using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading;
using System.Windows.Media;
using Ascon.Pilot.SDK.Controls.ObjectCardView;
using Ascon.Pilot.SDK.Extensions;
using Ascon.Pilot.Theme.ColorScheme;
using Ascon.Pilot.SDK.Menu;
using Ascon.Pilot.SDK.TaskSample.Extensions;
using Ascon.Pilot.SDK.TaskSample.Services;

namespace Ascon.Pilot.SDK.TaskSample
{
    [Export(typeof(IMenu<TasksViewContext2>))]
    public class TaskMenuSample : IMenu<TasksViewContext2>
    {
        private const string CREATE_NEW_TASK = "miCreateNewTask";
        private const string FROM_TEMPLATE = "miFromTemplate";
        private const string CREATE_NEW_WORKFLOW = "miCreateNewWorkflow";
        private const string SHOW_WORKFLOW_DIALOG_WITH_PREDEFINED_DATA = "miShowWorkflowDialogWithPredefinedData";
        private const string SHOW_TASK_DIALOG_WITH_PREDEFINED_DATA = "miShowTaskDialogWithPredefinedData";

        private readonly ITaskTypesService _taskTypesService;
        private readonly ITaskTemplateParser _taskTemplateParser;
        private readonly IObjectsRepository _repository;
        private readonly IPilotServices _pilotServices;
        private readonly IDialogService _dialogService;
        private readonly Dictionary<string, IType> _types = new Dictionary<string, IType>();
        private readonly Dictionary<string, TemplateItem> _templates = new Dictionary<string, TemplateItem>();

        [ImportingConstructor]
        public TaskMenuSample(
            IObjectsRepository repository,
            IPilotServices pilotServices,
            IDialogService dialogService,
            ITaskTypesService taskTypesService,
            ITaskTemplateParser taskTemplateParser)
        {
            _repository = repository;
            _pilotServices = pilotServices;
            _dialogService = dialogService;
            _taskTypesService = taskTypesService;
            _taskTemplateParser = taskTemplateParser;

            var accentColor = (Color) ColorConverter.ConvertFromString(pilotServices.PilotDialogService.AccentColor);
            ColorScheme.Initialize(accentColor, pilotServices.PilotDialogService.Theme);
        }

        public void Build(IMenuBuilder builder, TasksViewContext2 context)
        {
            builder.AddSeparator(2);

            var menu = builder.AddItem(CREATE_NEW_TASK, 3)
                .WithHeader("Create new task")
                .WithIcon(IconsService.CreateTaskIcon)
                .WithSubmenu();

            var taskTypes = _taskTypesService.RootTaskTypes;
            BuildSubMenu(taskTypes, menu);

            var fromTemplateMenu = builder.AddItem(FROM_TEMPLATE, 4)
                .WithHeader("From template")
                .WithSubmenu();

            BuildFromTemplateSubMenu(fromTemplateMenu);

            var wfMenu = builder.AddItem(CREATE_NEW_WORKFLOW, 5)
                .WithHeader("Create new workflow")
                .WithIcon(IconsService.CreateWorkflowIcon)
                .WithSubmenu();

            var workflowTypes = _taskTypesService.RootWorkflowTypes;
            BuildSubMenu(workflowTypes, wfMenu);

            builder.AddItem(SHOW_TASK_DIALOG_WITH_PREDEFINED_DATA, 6)
                .WithHeader("Show task dialog with predefined data")
                .WithIcon(IconsService.CreateTaskIcon);

            builder.AddItem(SHOW_WORKFLOW_DIALOG_WITH_PREDEFINED_DATA, 7)
                .WithHeader("Show workflow dialog with predefined data")
                .WithIcon(IconsService.CreateWorkflowIcon);

            if (builder.ItemNames.Count() > 7)
                builder.AddSeparator(8);
        }

        public void OnMenuItemClick(string name, TasksViewContext2 context)
        {
            if (name == SHOW_WORKFLOW_DIALOG_WITH_PREDEFINED_DATA)
            {
                _dialogService.ShowWorkflowDialogWithData(_taskTypesService);
                return;
            }

            if (name == SHOW_TASK_DIALOG_WITH_PREDEFINED_DATA)
            {
                _dialogService.ShowTaskDialogWithData(_taskTypesService);
                return;
            }

            if (_types.TryGetValue(name, out var type))
            {
                if (type.IsWorkflowType())
                {
                    _dialogService.ShowNewWorkflowDialog(type, _taskTypesService);
                    return;
                }

                if (type.IsTaskType())
                {
                    _dialogService.ShowNewTaskDialog(type);
                }

                return;
            }

            if (_templates.TryGetValue(name, out var template))
            {
                MakeTaskFromTemplate(template.TemplateStr);
            }
        }

        private void BuildSubMenu(IReadOnlyList<IType> types, IMenuBuilder menu)
        {
            for (var i = 0; i < types.Count; i++)
            {
                var type = types[i];
                var typeName = type.Name.GetXamlName();
                menu.AddItem(typeName, i).WithHeader(type.Title).WithIcon(type.SvgIcon);
                _types[typeName] = type;
            }
        }

        private async void BuildFromTemplateSubMenu(IMenuBuilder menu)
        {
            _templates.Clear();

            var loader = new ObjectLoader(_repository);
            var taskTemplatesRoot = await loader.Load(SystemObjectIds.TaskTemplateRootObjectId);
            var cts = new CancellationTokenSource();
            var firsLevelTemplates = await _repository.AsyncMethods().GetObjectsAsync(
                taskTemplatesRoot.Children,
                x => x.Type.Name == SystemTypeNames.TASK_TEMPLATE ? new TemplateItem(x) : null,
                cts.Token);

            var i = 0;
            foreach (var template in firsLevelTemplates.Where(x => x != null))
            {
                menu.AddItem(template.XmlName, i).WithHeader(template.Title);
                _templates.Add(template.XmlName, template);
                i++;
            }
        }

        private void MakeTaskFromTemplate(string templateStr)
        {
            var template = _taskTemplateParser.Parse(templateStr);
            if (template == null)
                return;

            var itemType = _types.FirstOrDefault(x => x.Value.Id == template.TypeId).Value;
            if (itemType == null)
                return;

            if (itemType.IsTaskType())
            {
                MakeSimpleTaskFromTemplate(template, itemType);
                return;
            }

            if (itemType.IsWorkflowType())
            {
                MakeWorkflowFromTemplate(template, itemType);
                return;
            }
        }

        private void MakeSimpleTaskFromTemplate(ITaskTemplateItem template, IType taskType)
        {
            var modifier = _pilotServices.ObjectModifier;
            var builder = modifier.CreateTask(taskType);

            foreach (var pair in template.Attributes)
            {
                var attributeName = pair.Key;
                if (attributeName == SystemTaskAttributes.DEADLINE_DATE)
                    continue;

                var value = pair.Value;

                if (attributeName == SystemTaskAttributes.DEADLINE_DATE_FOR_TEMPLATE)
                {
                    attributeName = SystemTaskAttributes.DEADLINE_DATE;
                    value = DeadlineSplit.Parse(value as string).GetNewDeadlineFromNow();
                }

                builder.SetAttributeValue(attributeName, new DValue {Value = value});
            }

            var person = _repository.GetCurrentPerson();
            builder.SetInitiator(person.MainPosition.Position);
            builder.SetAttachments(template.Attachments, modifier);

            modifier.Apply();
        }

        private void MakeWorkflowFromTemplate(ITaskTemplateItem template, IType workflowType)
        {
            var person = _pilotServices.ObjectsRepository.GetCurrentPerson();
            var modifier = _pilotServices.ObjectModifier;

            // create workflow
            var workflowBuilder = modifier.CreateWorkflow(workflowType, Guid.Empty); //Create in tasks root
            foreach (var pair in template.Attributes)
            {
                var attributeName = pair.Key;
                var value = new DValue {Value = pair.Value};
                workflowBuilder.SetAttributeValue(attributeName, value);
            }

            workflowBuilder.SetInitiator(person.MainPosition.Position);

            // create stages and tasks
            var stageIndex = 0;
            foreach (var stage in template.Children)
            {
                var stageType = _repository.GetType(stage.TypeId);
                if(!stageType.IsWorkflowStageType())
                    continue;

                var stageBuilder = modifier.CreateStage(stageIndex, workflowBuilder.DataObject.Id, stageType)
                    .SetInitiator(person.MainPosition.Position);

                foreach (var task in stage.Children)
                {
                    var taskType = _repository.GetType(task.TypeId);
                    var taskBuilder = modifier.CreateTask(taskType, stageBuilder.DataObject.Id)
                        .SetAutocompleteParent(true);

                    // fill task for stage
                    foreach (var pair in task.Attributes)
                    {
                        var attributeName = pair.Key;
                        if (attributeName == SystemTaskAttributes.DEADLINE_DATE)
                            continue;

                        var value = pair.Value;

                        if (attributeName == SystemTaskAttributes.DEADLINE_DATE_FOR_TEMPLATE)
                        {
                            attributeName = SystemTaskAttributes.DEADLINE_DATE;
                            value = DeadlineSplit.Parse(value as string).GetNewDeadlineFromNow();
                        }

                        taskBuilder.SetAttributeValue(attributeName, new DValue { Value = value });
                    }

                    taskBuilder.SetInitiator(person.MainPosition.Position);
                    taskBuilder.SetAttachments(task.Attachments, modifier);
                }

                stageIndex++;
            }

            modifier.Apply();
        }

        class TemplateItem
        {
            public TemplateItem(IDataObject source)
            {
                var name = source.Attributes[SystemAttributeNames.TASK_TEMPLATE_NAME] as string;
                XmlName = name.GetXamlName();
                Title = name;
                TemplateStr = source.Attributes[SystemAttributeNames.TASK_TEMPLATE_VALUE] as string;
            }

            public string XmlName { get; }
            public string Title { get; }
            public string TemplateStr { get; }
        }
    }
}