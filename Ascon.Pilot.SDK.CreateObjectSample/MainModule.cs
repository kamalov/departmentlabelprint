﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Ascon.Pilot.SDK.Menu;

namespace Ascon.Pilot.SDK.CreateObjectSample
{
    [Export(typeof(IMenu<ObjectsViewContext>))]
    public class MainModule : IMenu<ObjectsViewContext>
    {
        private readonly IObjectsRepository _repository;
        private readonly IObjectModifier _modifier;
        private readonly ISearchService _searchService;
        private const string CREATE_COMMAND = "CreateCommand";
        private const string CREATE_SHORTCUT = "CreateShortcutCommand";
        private const string CREATE_SMART_FOLDER = "CreateSmartFolderCommand";
        private readonly List<Guid> _selectionIds = new List<Guid>();
        private Guid _firstParentId;
        
        [ImportingConstructor]
        public MainModule(IObjectsRepository repository, IObjectModifier modifier, ISearchService searchService)
        {
            _repository = repository;
            _modifier = modifier;
            _searchService = searchService;
        }

        public void Build(IMenuBuilder builder, ObjectsViewContext context)
        {
            var createItemBuilder = builder.AddItem(CREATE_COMMAND, 0).WithHeader("Create custom object").WithSubmenu();

            var selectionList = context.SelectedObjects.ToList();
            _firstParentId = selectionList.First().ParentId;

            if (selectionList.Any())
            {
                createItemBuilder.AddItem(CREATE_SHORTCUT, 0).WithHeader("Create shortcut");
                _selectionIds.Clear();
                foreach (var dataObject in selectionList)
                {
                    _selectionIds.Add(dataObject.Id);
                }
            }

            if (!context.IsContext)
                return;

            createItemBuilder.AddItem(CREATE_SMART_FOLDER, 1).WithHeader("Create smart folder");
        }

        public async void OnMenuItemClick(string name, ObjectsViewContext context)
        {
            if (name == CREATE_SHORTCUT)
            {
                var loader = new ObjectLoader(_repository);
                var parent = await loader.Load(_firstParentId);
                
                foreach (var id in _selectionIds)
                {
                    var value = id.ToString();
                    _modifier.Create(parent, GetShortcutType()).SetAttribute(SystemAttributeNames.SHORTCUT_OBJECT_ID, value);
                }

                if (_selectionIds.Any())
                    _modifier.Apply();
            }

            if (name == CREATE_SMART_FOLDER)
            {
                var loader = new ObjectLoader(_repository);
                var parent = await loader.Load(_firstParentId);

                _modifier.Create(parent, GetSmartFolderType())
                    .SetAttribute(SystemAttributeNames.SMART_FOLDER_TITLE, "title")
                    .SetAttribute(SystemAttributeNames.SEARCH_CRITERIA, GetSmartFolderSearchString())
                    .SetAttribute(SystemAttributeNames.SEARCH_CONTEXT_OBJECT_ID, SystemObjectIds.RootObjectId.ToString());
                _modifier.Apply();
                
            }
        }

        private IType GetShortcutType()
        {
            return _repository.GetType(SystemTypeNames.SHORTCUT);
        }

        private IType GetSmartFolderType()
        {
            return _repository.GetType(SystemTypeNames.SMART_FOLDER);
        }

        private string GetSmartFolderSearchString()
        {
            var builder = _searchService.GetSmartFolderQueryBuilder();
            var type = _repository.GetType("Project");
            if (type != null)
                return builder.WithSearchMode(SearchMode.Attributes)
                              .WithType(type.Id) 
                              .WithKeyword("5000").ToString();
            return string.Empty;
        }
    }
}
