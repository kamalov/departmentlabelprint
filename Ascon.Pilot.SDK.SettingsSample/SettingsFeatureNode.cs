﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Ascon.Pilot.SDK.SettingsSample
{
    class SettingsFeatureNode
    {
        private readonly ObservableCollection<string> _settings = new ObservableCollection<string>();

        public SettingsFeatureNode(string featureTitle)
        {
            FeatureTitle = featureTitle;
        }

        public ObservableCollection<string> Settings
        {
            get { return _settings; }
        }
        public string FeatureTitle { get; private set; }
    }
}
