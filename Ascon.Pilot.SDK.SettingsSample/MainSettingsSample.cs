﻿using System.ComponentModel.Composition;
using System.Linq;
using System.Windows.Media;
using Ascon.Pilot.Theme.ColorScheme; 

namespace Ascon.Pilot.SDK.SettingsSample
{
    [Export(typeof(IMainMenu))]
    public class MainSettingsSample : IMainMenu
    {
        private readonly IObjectsRepository _objectsRepository;
        private readonly IPersonalSettings _personalSettings;

        [ImportingConstructor]
        public MainSettingsSample(IObjectsRepository objectsRepository, IPilotDialogService dialogService, IPersonalSettings personalSettings)
        {
            _objectsRepository = objectsRepository;
            var convertFromString = ColorConverter.ConvertFromString(dialogService.AccentColor);
            if (convertFromString != null)
            {
                var accentColor = (Color)convertFromString;
                ColorScheme.Initialize(accentColor, dialogService.Theme);
            }

            _personalSettings = personalSettings;
        }

        public void OnMenuItemClick(string itemName)
        {
            if (itemName == "miMySettings")
            {
                var model = new SettingsViewModel(_personalSettings, _objectsRepository);
                var settingsView = new SettingsDialog() { DataContext = model };
                settingsView.ShowDialog();
            }
        }

        public void BuildMenu(IMenuHost menuHost)
        {
            var menuItem = menuHost.GetItems().First();
            menuHost.AddSubItem(menuItem, "miMySettings", "My settings", null, 2);
        }
    }
}
