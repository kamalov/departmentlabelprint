﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Ascon.Pilot.SDK.SettingsSample
{
    class SettingsViewModel : INotifyPropertyChanged, IObserver<KeyValuePair<string, string>>, IObserver<IDataObject>
    {
        private readonly IObjectsRepository _repository;
        private readonly ObservableCollection<SettingsFeatureNode> _settings = new ObservableCollection<SettingsFeatureNode>();
        private readonly SettingsFeatureNode _favoritesFeatureNode;
        private readonly SettingsFeatureNode _rolesFeatureNode;
        private readonly SettingsFeatureNode _driveLeterFeatureNode;
        private readonly SettingsFeatureNode _sampleFeatureNode;
        private readonly SettingsFeatureNode _sample2FeatureNode;


        public event PropertyChangedEventHandler PropertyChanged;

        public SettingsViewModel(IPersonalSettings personalSettings, IObjectsRepository repository)
        {
            _repository = repository;
            _favoritesFeatureNode = new SettingsFeatureNode("Favorites");
            _rolesFeatureNode = new SettingsFeatureNode("Roles");
            _driveLeterFeatureNode = new SettingsFeatureNode("Drive leter");
            _sampleFeatureNode = new SettingsFeatureNode("Sample feature");
            _sample2FeatureNode = new SettingsFeatureNode("Sample feature 2");
            
            _settings.Add(_favoritesFeatureNode);
            _settings.Add(_rolesFeatureNode);
            _settings.Add(_driveLeterFeatureNode);
            _settings.Add(_sampleFeatureNode);
            _settings.Add(_sample2FeatureNode);


            personalSettings.SubscribeSetting(SystemSettingsKeys.FavoritesFeatureKey).Subscribe(this);
            personalSettings.SubscribeSetting(SystemSettingsKeys.AgreementRolesFeatureKey).Subscribe(this);
            personalSettings.SubscribeSetting(SystemSettingsKeys.PilotStorageDriveLetter).Subscribe(this);
            personalSettings.SubscribeSetting(SettingsFeatureKeys.SampleFeatureKey).Subscribe(this);
            personalSettings.SubscribeSetting(SettingsFeatureKeys.Sample2FeatureKey).Subscribe(this);
        }

        public ObservableCollection<SettingsFeatureNode> Settings
        {
            get { return _settings; }
        }

        public void OnNext(KeyValuePair<string, string> value)
        {
            if (value.Value == null)
                return;
            
            if (value.Key == SystemSettingsKeys.FavoritesFeatureKey)
            {
                var ids = new List<Guid>();
                var idsArr = value.Value.Split(';');
                foreach (var s in idsArr)
                {
                    Guid id;
                    if (Guid.TryParse(s, out id))
                    {
                        ids.Add(id);
                    }
                }
                
                _repository.SubscribeObjects(ids).Subscribe(this);
            }

            if (value.Key == SystemSettingsKeys.AgreementRolesFeatureKey)
            {
                var rolesArr = value.Value.Split(';');
                foreach (var s in rolesArr)
                {
                   if (s == ";")
                       continue;

                   _rolesFeatureNode.Settings.Remove(s);
                   _rolesFeatureNode.Settings.Add(s);
                }
            }

            if (value.Key == SystemSettingsKeys.PilotStorageDriveLetter)
            {
                _driveLeterFeatureNode.Settings.Add(value.Value);
            }

            if (value.Key == SettingsFeatureKeys.SampleFeatureKey)
            {
                _sampleFeatureNode.Settings.Add(value.Value);
            }

            if (value.Key == SettingsFeatureKeys.Sample2FeatureKey)
            {
                _sample2FeatureNode.Settings.Add(value.Value);
            }
        }

        public void OnNext(IDataObject value)
        {
            _favoritesFeatureNode.Settings.Add(value.DisplayName);
        }

        public void OnError(Exception error)
        {
            
        }

        public void OnCompleted()
        {
            
        }
    }
}
