﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Ascon.Pilot.SDK.SettingsSample
{
    class SettingsFeatureEditorViewModel : INotifyPropertyChanged
    {
        private readonly ISettingValueProvider _valueProvider;
        public event PropertyChangedEventHandler PropertyChanged;
        private string _selectedItem;
        private readonly List<string> _items = new List<string>();

        public SettingsFeatureEditorViewModel(ISettingValueProvider valueProvider)
        {
            _valueProvider = valueProvider;
            _items.Add("ru-RU");
            _items.Add("en-US");
            _items.Add("de-DE");

            _selectedItem = valueProvider.GetValue();
        }

        public string SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                _valueProvider.SetValue(value);
                NotifyPropertyChanged("SelectedItem");
            }
        }

        public List<string> Items
        {
            get { return _items; }
        }

        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
