﻿using System.ComponentModel.Composition;
using System.Windows;

namespace Ascon.Pilot.SDK.SettingsSample
{
    [Export(typeof(ISettingsFeature))]
    public class SampleSettingsFeature : ISettingsFeature
    {
        private ISettingValueProvider _settingValueProvider;

        public void SetValueProvider(ISettingValueProvider settingValueProvider)
        {
            _settingValueProvider = settingValueProvider;
        }

        public string Key
        {
            get { return SettingsFeatureKeys.SampleFeatureKey; }
        }

        public string Title
        {
            get { return "Sample setting"; }
        }

        public FrameworkElement Editor
        {
            get
            {
                var editorViewModel = new SettingsFeatureEditorViewModel(_settingValueProvider);
                var editor = new SettingFeatureEditor
                {
                    DataContext = editorViewModel
                };
                return editor;
            }
        }

        
    }

    [Export(typeof(ISettingsFeature))]
    public class SampleSettingsFeature2 : ISettingsFeature
    {
        public void SetValueProvider(ISettingValueProvider settingValueProvider)
        {
            
        }

        public string Key
        {
            get { return SettingsFeatureKeys.Sample2FeatureKey; }
        }

        public string Title
        {
            get { return "Sample setting 2"; }
        }

        public FrameworkElement Editor
        {
            get { return null; }
        }
    }
}
