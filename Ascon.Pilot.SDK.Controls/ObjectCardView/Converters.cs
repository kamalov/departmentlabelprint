﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows;
using Ascon.Pilot.Theme.Tools;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    public class StringFormatConverter : BaseValueConverter
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length < 2)
                return null;

            var value = values[0];
            var format = values[1] as string;

            string cultureName = null;
            if (values.Length > 2)
                cultureName = values[2] as string;

            if (value == null || string.IsNullOrEmpty(format))
                return null;

            try
            {
                var cultureInfo = !string.IsNullOrEmpty(cultureName)
                    ? new CultureInfo(cultureName)
                    : CultureInfo.CurrentUICulture;

                return string.Format(cultureInfo, format, value);
            }
            catch
            {
                return format;
            }
        }
    }

    public class NullableLongToStringConverter : BaseValueConverter
    {
        protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var longValue = value as long?;
            return longValue != null ? longValue.ToString() : null;
        }

        protected override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strValue = (string)value;
            if (string.IsNullOrEmpty(strValue))
                return null;
            return long.Parse(strValue);
        }

        private string _format = string.Empty;
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length == 1)
                return Convert(values[0], targetType, parameter, culture);

            var longValue = values[0] as long?;
            _format = values[1] as string;
            return longValue != null ? longValue.Value.ToString(_format) : null;
        }

        public override object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            if (targetTypes.Length == 1)
                return new[] { ConvertBack(value, targetTypes[0], parameter, culture) };
            string strValue = (string)value;
            if (string.IsNullOrEmpty(strValue))
                return null;
            long parsedLong;
            return long.TryParse(strValue, out parsedLong) ? new object[] { parsedLong, _format } : null;
        }
    }

    public class NullableDoubleToStringConverter : BaseValueConverter
    {
        protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double? doubleValue = value as double?;
            return doubleValue != null ? doubleValue.Value.ToString(Thread.CurrentThread.CurrentUICulture) : null;
        }

        protected override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strValue = (string)value;
            if (string.IsNullOrEmpty(strValue))
                return null;
            return double.Parse(strValue, Thread.CurrentThread.CurrentUICulture);
        }
    }

    public class NullableDecimalToStringConverter : BaseValueConverter
    {
        protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            decimal? decimalValue = value as decimal?;
            return decimalValue != null ? decimalValue.Value.ToString(Thread.CurrentThread.CurrentUICulture) : null;
        }

        protected override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strValue = (string)value;
            if (string.IsNullOrEmpty(strValue))
                return null;
            return decimal.Parse(strValue, Thread.CurrentThread.CurrentUICulture);
        }
    }

    public class StringValueConverter : BaseValueConverter
    {
        protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strValue = value as string;
            return !string.IsNullOrEmpty(strValue) ? strValue : null;
        }

        protected override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string strValue = (string)value;
            return !string.IsNullOrEmpty(strValue) ? strValue : null;
        }
    }

    public class DisplayHeightToTextWrappingConverter : BaseValueConverter
    {
        protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int displayHeight = (int)value;
            return displayHeight > 1 ? TextWrapping.Wrap : TextWrapping.NoWrap;
        }
    }

    class NumeratorFormatConverter : BaseValueConverter
    {
        public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length != 3)
                return null;

            var formatString = values[0] as string;
            var attributeValues = values[1] as IDictionary<string, DValue>;
            var attributeFormatParser = (IAttributeFormatParser) values[2];

            if (string.IsNullOrEmpty(formatString))
                return null;

            if(attributeValues == null)
                attributeValues = new Dictionary<string, DValue>();

            return attributeFormatParser.PreviewNumeratorFormat(formatString, attributeValues.ToDictionary(x => x.Key, x => x.Value.ToString()));
        }
    }

    public class DateTimePickerFormatStringConverter : BaseValueConverter
    {
        protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var format = (string)value;
            if (string.IsNullOrEmpty(format))
                return null;
            return format.Replace("{0:", "").Replace("}", "");
        }
    }
}
