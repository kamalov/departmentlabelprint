﻿using System;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView.ReferenceBook
{
    public class ListViewTypeFilter 
    {
        private readonly Func<IObjectsRepository> _getRepository;

        public ListViewTypeFilter(Func<IObjectsRepository> getRepository)
        {
            _getRepository = getRepository;
        }

        public bool Matches(int typeId)
        {
            var type = _getRepository().GetType(typeId);
            return Matches(type);
        }

        private bool Matches(IType type)
        {
            if (type.Name == SystemTypeNames.SMART_FOLDER)
                return true;
            if (type.Name == SystemTypeNames.SHORTCUT)
                return true;
            return !type.IsService && type.Kind == TypeKind.User;
        }
    }
}
