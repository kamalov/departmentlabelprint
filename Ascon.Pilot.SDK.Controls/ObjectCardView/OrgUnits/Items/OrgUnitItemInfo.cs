﻿namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    public class OrgUnitItemInfo
    {
        public OrgUnitItemInfo(int positionId)
        {
            PositionId = positionId;
        }

        public int PositionId { get; }
    }
}
