﻿using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using Ascon.Pilot.Theme.Controls;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    public class StageLayoutRootGrid : Grid
    {
        public static readonly DependencyProperty StageNameProperty = DependencyProperty.Register(
            "StageName", typeof(string), typeof(StageLayoutRootGrid), new PropertyMetadata(default(string)));

        public string StageName
        {
            get { return (string)GetValue(StageNameProperty); }
            set { SetValue(StageNameProperty, value); }
        }

        public static readonly DependencyProperty StageIndexProperty = DependencyProperty.Register(
            "StageIndex", typeof(int), typeof(StageLayoutRootGrid), new PropertyMetadata(default(int)));

        public int StageIndex
        {
            get { return (int) GetValue(StageIndexProperty); }
            set { SetValue(StageIndexProperty, value); }
        }

        protected override AutomationPeer OnCreateAutomationPeer()
        {
            return new AlwaysVisibleUiElementAutomationPeer(this);
        }
    }
}
