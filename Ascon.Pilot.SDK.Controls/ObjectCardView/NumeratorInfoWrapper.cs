﻿namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    public class NumeratorInfoWrapper
    {
        public string DisplayName { get; private set; }
        public string Configuration { get; private set; }

        public NumeratorInfoWrapper(INumeratorInfo numeratorInfo)
        {
            DisplayName = numeratorInfo.DisplayName;
            Configuration = numeratorInfo.Configuration;
        }
    }
}