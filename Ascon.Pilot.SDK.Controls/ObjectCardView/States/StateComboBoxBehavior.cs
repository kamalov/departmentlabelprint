﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView.States
{
    static class StateComboBoxBehavior
    {
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.RegisterAttached("ViewModel", typeof(CardControlViewModel), typeof(StateComboBoxBehavior), new PropertyMetadata(null, OnViewModelChanged));

        public static CardControlViewModel GetViewModel(DependencyObject obj)
        {
            return (CardControlViewModel)obj.GetValue(ViewModelProperty);
        }

        public static void SetViewModel(DependencyObject obj, CardControlViewModel value)
        {
            obj.SetValue(ViewModelProperty, value);
        }

        private static void OnViewModelChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var comboBox = d as ComboBox;
            if (comboBox == null)
                throw new NotSupportedException();

            var viewModel = (CardControlViewModel)e.NewValue;

            comboBox.Loaded += (o, a) =>
            {
                comboBox.ItemsSource = GetStatuses(viewModel);
                comboBox.IsEditable = false;
                comboBox.DisplayMemberPath = nameof(StateItem.Title);
                comboBox.SelectedValuePath = nameof(StateItem.Name);
                comboBox.SetBinding(Selector.SelectedValueProperty, new Binding(nameof(CardControlViewModel.Value)));
                comboBox.IsTextSearchCaseSensitive = false;

                if (comboBox.HasItems && comboBox.SelectedItem == null)
                    comboBox.SelectedItem = comboBox.Items[0];
            };
        }

        private static IEnumerable GetStatuses(CardControlViewModel viewModel)
        {
            var repository = viewModel.Repository;
            var configuration = StateConfigurationSerializer.Deserialize(viewModel.Attribute.Configuration);
            var items = new List<StateItem>();

            if (!viewModel.Attribute.IsObligatory)
                items.Add(new EmptyStateItem());

            var states = repository.GetUserStates().ToList();
            foreach (var stateName in configuration.UserStates)
            {
                var state = states.FirstOrDefault(s => !s.IsDeleted && stateName != null && s.Name.Equals(stateName, StringComparison.OrdinalIgnoreCase));
                if (state != null)
                    items.Add(new StateItem(state));
            }

            return items;
        }
    }
}
