﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView.States
{
    class StateObjectControl : Control
    {
        static StateObjectControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(StateObjectControl), new FrameworkPropertyMetadata(typeof(StateObjectControl)));
        }

        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(StateObjectControl), new PropertyMetadata(default(string)));

        public static readonly DependencyProperty IconProperty =
            DependencyProperty.Register("Icon", typeof(ImageSource), typeof(StateObjectControl), new PropertyMetadata(default(ImageSource)));

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public ImageSource Icon
        {
            get { return (ImageSource)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }
    }
}
