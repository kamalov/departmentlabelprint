﻿using System;
using System.Globalization;
using System.Windows.Media;
using Ascon.Pilot.SDK.Controls.Converters;
using Ascon.Pilot.Theme.Tools;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView.States
{
    class UserStateIconConverter: BaseValueConverter
    {
        protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var type = value as IUserState;
            return type == null ? null : Convert(type);
        }

        public static ImageSource Convert(IUserState state)
        {
            return ConvertAndFreeze(state.Icon);
        }

        private static ImageSource ConvertAndFreeze(byte[] bytes)
        {
            var imageSource = ByteImageConverter.Convert(bytes);
            imageSource?.Freeze();
            return imageSource;
        }
    }
}
