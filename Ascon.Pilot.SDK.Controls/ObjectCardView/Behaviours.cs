﻿using System;
using System.Globalization;
using System.Windows;
using Ascon.Pilot.SDK.Controls.Commands;
using Ascon.Pilot.Theme.Controls;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    public class LongEditBehavior
    {
        public static readonly DependencyProperty EnableProperty = DependencyProperty.RegisterAttached(
            "Enable", typeof(bool), typeof(LongEditBehavior), new PropertyMetadata(false, OnEnableChanged));

        public static void SetEnable(DependencyObject element, bool value)
        {
            element.SetValue(EnableProperty, value);
        }

        public static bool GetEnable(DependencyObject element)
        {
            return (bool) element.GetValue(EnableProperty);
        }

        private static void OnEnableChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var editor = (ButtonEdit)sender;
            editor.LeftCommandParameter = editor;
            editor.RightCommandParameter = editor;
            editor.LeftCommand = new DelegateCommand<ButtonEdit>(DecrementValue);
            editor.RightCommand = new DelegateCommand<ButtonEdit>(IncrementValue);
        }

        private static void IncrementValue(ButtonEdit buttonEdit)
        {
            UpdateValue(buttonEdit, x => ++x);
        }

        private static void DecrementValue(ButtonEdit buttonEdit)
        {
            UpdateValue(buttonEdit, x => --x);
        }

        private static void UpdateValue(ButtonEdit buttonEdit, Func<long, long> func)
        {
            long value;
            if (long.TryParse(buttonEdit.Text, out value))
            {
                value = func(value);
            }
            buttonEdit.Text = value.ToString(CultureInfo.InvariantCulture);
        }
    }

    public class RemoveLineBreaksOnPastingBehavior
    {
        public static readonly DependencyProperty EnabledProperty = DependencyProperty.RegisterAttached(
            "Enabled", typeof(bool), typeof(RemoveLineBreaksOnPastingBehavior), new PropertyMetadata(false, OnEnableChanged));

        public static void SetEnabled(DependencyObject element, bool value)
        {
            element.SetValue(EnabledProperty, value);
        }

        public static bool GetEnabled(DependencyObject element)
        {
            return (bool) element.GetValue(EnabledProperty);
        }
        private static void OnEnableChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var enable = e.NewValue is bool && (bool) e.NewValue;
            if(enable)
                DataObject.AddPastingHandler(sender, OnPaste);
            else
                DataObject.RemovePastingHandler(sender, OnPaste);
        }

        private static void OnPaste(object sender, DataObjectPastingEventArgs e)
        {
            bool isUnicode = e.DataObject.GetDataPresent(ToDataFormats(TextDataFormat.UnicodeText), false);
            string dataFormat = ToDataFormats(isUnicode ? TextDataFormat.UnicodeText : TextDataFormat.Text);
            string clipboard = (string)e.DataObject.GetData(dataFormat);

            string result = clipboard.Replace(Environment.NewLine, " ");

            DataObject d = new DataObject();
            d.SetData(dataFormat, result);
            e.DataObject = d;
        }

        private static string ToDataFormats(TextDataFormat format)
        {
            switch (format)
            {
                case TextDataFormat.Text:
                    return DataFormats.Text;

                case TextDataFormat.UnicodeText:
                    return DataFormats.UnicodeText;

                case TextDataFormat.Rtf:
                    return DataFormats.Rtf;

                case TextDataFormat.Html:
                    return DataFormats.Html;

                case TextDataFormat.CommaSeparatedValue:
                    return DataFormats.CommaSeparatedValue;
            }

            return DataFormats.UnicodeText;
        }

    }
}
