﻿using System;
using System.Globalization;
using Ascon.Pilot.Theme.Tools;

namespace Ascon.Pilot.SDK.Controls.ObjectCardView
{
    public class NumeratorValueConverter : BaseValueConverter
    {
        protected override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var nodeValue = value as NumeratorInfoWrapper;
            return nodeValue == null ? null : nodeValue.Configuration;
        }

        protected override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var nodeValue = value as NumeratorInfoWrapper;
            return nodeValue == null ? null : nodeValue.Configuration;
        }
    }
}
