﻿using System;

namespace Ascon.Pilot.SDK.Controls.Tools
{
    class NaturalComparer
    {
        private static int ExtractNumber(string str, int index, out Int64 result)
        {
            result = 0;
            int i = index;
            while (i < str.Length)
            {
                if (Char.IsDigit(str[i]))
                {
                    result = result * 10 + (str[i] - '0');
                    i++;
                }
                else
                    break;
            }
            return i;
        }

        public static int Compare(string str1, string str2)
        {
            if (str1 == null && str2 == null)
                return 0;
            if (str1 == null)
                return -1;
            if (str2 == null)
                return 1;

            int i1 = 0;
            int i2 = 0;
            while (i1 < str1.Length && i2 < str2.Length)
            {
                if (Char.IsDigit(str1[i1]) && Char.IsDigit(str2[i2]))
                {
                    Int64 num1;
                    Int64 num2;
                    i1 = ExtractNumber(str1, i1, out num1);
                    i2 = ExtractNumber(str2, i2, out num2);
                    var result = num1.CompareTo(num2);
                    if (result != 0)
                        return result;
                    result = i2.CompareTo(i1);
                    if (result != 0)
                        return result;
                }
                else
                {
                    var leftChar = Char.ToUpper(str1[i1]);
                    var rightChar = Char.ToUpper(str2[i2]);
                    var result = leftChar.CompareTo(rightChar);
                    if (result != 0)
                        return result;
                    i1++;
                    i2++;
                }
            }
            if (i1 < str1.Length) return 1;
            if (i2 < str2.Length) return -1;
            return 0;
        }
    }
}
