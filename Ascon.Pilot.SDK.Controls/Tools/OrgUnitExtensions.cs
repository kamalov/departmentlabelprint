﻿using System.Linq;

namespace Ascon.Pilot.SDK.Controls.Tools
{
    public static class OrgUnitExtensions
    {
        public static IPerson GetPersonOnOrganizationUnit(this IObjectsRepository repository, int id)
        {
            var people = repository.GetPeople();
            return people.Where(p => p.Positions.Select(m => m.Position).Contains(id))
                .OrderBy(o => o.Positions.First(x => x.Position == id).Order)
                .FirstOrDefault();
        }

    }
}