﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using SharpVectors.Runtime;

namespace Ascon.Pilot.SDK.Controls.Converters
{
    public class ByteImageConverter : IValueConverter
    {
        #region IValueConverter Members

        public static DrawingImage Convert(byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0)
                return null;

            if (!SvgConverterHelper.TryConvert(bytes, out var drawing))
                return null;

            var rect = GetDrawingRect(drawing);

            DrawingBrush brush = new DrawingBrush(drawing)
            {
                Stretch = Stretch.None,
                Viewbox = rect,
                ViewboxUnits = BrushMappingMode.Absolute
            };
                    
            DrawingImage image = new DrawingImage(new GeometryDrawing
            {
                Brush = brush,
                Geometry = new RectangleGeometry(rect)
            });

            return image;
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var key = value as byte[];
            if (key != null)
            {
                return Convert(key);
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

        static Rect GetDrawingRect(DrawingGroup rootGroup)
        {
            var result = GetDrawingRectRecursive(rootGroup);
            if (!result.HasValue)
                throw new InvalidOperationException("Ошибка при попытке получить размеры drawing group");

            return result.Value;
        }

        // Данный метод основывается на деталях реализации SharpVectors конвертора
        // Предполагается, что реальные размеры DrawingGroup хранятся в ClipGeometry
        // группы, помеченной ключом SvgObject.DrawLayer
        static Rect? GetDrawingRectRecursive(DrawingGroup group)
        {
            var key = SvgLink.GetKey(group);
            if (key == SvgObject.DrawLayer)
            {
                if (group.ClipGeometry is RectangleGeometry geometry)
                {
                    return geometry.Bounds;
                }
            }
            foreach (var childGroup in group.Children.OfType<DrawingGroup>())
            {
                var res = GetDrawingRectRecursive(childGroup);
                if (res != null)
                {
                    return res;
                }
            }
            return null;
        }
    }

}
