﻿namespace Ascon.Pilot.SDK.NotificationsSample
{
    public static class NotificationExtension
    {
        public static string GetActionString(this INotification notification)
        {
            switch (notification.ChangeKind)
            {
                case NotificationKind.ObjectCreated:
                    return Resources.Created;
                case NotificationKind.ObjectDeleted:
                    return Resources.Deleted;
                case NotificationKind.ObjectMoved:
                    return Resources.Moved;
                case NotificationKind.ObjectRenamed:
                    return Resources.Renamed;
                case NotificationKind.ObjectRestored:
                    return Resources.Restored;
                case NotificationKind.ObjectAccessChanged:
                    return Resources.AccessChanged;
                case NotificationKind.ObjectAttributeChanged:
                    return Resources.AttributeChanged;
                case NotificationKind.ObjectFileChanged:
                    return Resources.FileChanged;
                case NotificationKind.ObjectSignatureChanged:
                    return Resources.SignatureChanged;
                case NotificationKind.ObjectAnnotationAdded:
                    return Resources.AnnotationAdded;
                case NotificationKind.ObjectAnnotationDeleted:
                    return Resources.AnnotationDeleted;
                case NotificationKind.ObjectAnnotationChanged2:
                    return Resources.AnnotationChanged;
                case NotificationKind.ObjectAnnotationMessageAdded:
                    return Resources.AnnotationMessageAdded;
                case NotificationKind.ObjectAnnotationMessageDeleted:
                    return Resources.AnnotationMessageDeleted;
                case NotificationKind.ObjectAnnotationMessageChanged:
                    return Resources.AnnotationMessageChanged;
                case NotificationKind.TaskCreated:
                    return Resources.TaskCreated;
                case NotificationKind.TaskMessageChanged:
                    return Resources.MessageCreated;
                case NotificationKind.TaskAttachmentChanged:
                    return Resources.TaskAttachmentChanged;
                case NotificationKind.TaskTitleChanged:
                    return Resources.TaskTitleChanged;
                case NotificationKind.TaskDescriptionChanged:
                    return Resources.TaskDescriptionChanged;
                case NotificationKind.TaskStateAssigned:
                    return Resources.StateAssigned;
                case NotificationKind.TaskStateInProgress:
                    return Resources.StateInProgress;
                case NotificationKind.TaskStateOnValidation:
                    return Resources.StateOnValidation;
                case NotificationKind.TaskStateReturnedAfterValidation:
                    return Resources.StateReturnedAfterValidation;
                case NotificationKind.TaskStateRevoked:
                    return Resources.StateRevoked;
                case NotificationKind.ObjectUnlocked:
                    return Resources.Unlocked;
                case NotificationKind.TaskDeadlineChanged:
                    return Resources.DeadlineChanged;
                case NotificationKind.ObjectFreezed:
                    return Resources.Freezed;
                case NotificationKind.ObjectUnfreezed:
                    return Resources.Unfreezed;
                default:
                    return string.Empty;
            }
        }
    }
}