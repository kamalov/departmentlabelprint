﻿using System;
using System.ComponentModel.Composition;
using System.Windows;
using Ascon.Pilot.SDK.CreateObjectSample;

namespace Ascon.Pilot.SDK.NotificationsSample
{
    [Export(typeof(IDataPlugin))]
    public class Main : IDataPlugin, IObserver<INotification>
    {
        private readonly IObjectsRepository _repository;

        [ImportingConstructor]
        public Main(IObjectsRepository repository)
        {
            _repository = repository;
            repository.SubscribeNotification(NotificationKind.ObjectCreated).Subscribe(this);
            repository.SubscribeNotification(NotificationKind.ObjectDeleted).Subscribe(this);
            repository.SubscribeNotification(NotificationKind.StorageObjectCreated).Subscribe(this);
            repository.SubscribeNotification(NotificationKind.StorageObjectDeleted).Subscribe(this);
            repository.SubscribeNotification(NotificationKind.StorageObjectRenamed).Subscribe(this);
        }

        public async void OnNext(INotification value)
        {
            if (value.ChangeKind == NotificationKind.ObjectCreated)
            {
                var loader = new ObjectLoader(_repository);
                var obj = await loader.Load(value.ObjectId, value.ChangesetId());
                var message = string.Format("Object {0} has been created", obj.DisplayName);
                MessageBox.Show(message, "Ascon.Pilot.SDK.NotificationsSample");    
                return;
            }

            if (value.ChangeKind == NotificationKind.ObjectDeleted)
            {
                var loader = new ObjectLoader(_repository);
                var obj = await loader.Load(value.ObjectId, value.ChangesetId());
                var message = string.Format("Object {0} has been deleted", obj.DisplayName);
                MessageBox.Show(message, "Ascon.Pilot.SDK.NotificationsSample");
                return;
            }

            if (value.ChangeKind == NotificationKind.StorageObjectCreated)
            {
                var loader = new ObjectLoader(_repository);
                var obj = await loader.Load(value.ObjectId, value.ChangesetId());
                var message = string.Format("Storage object {0} has been created", obj.DisplayName);
                MessageBox.Show(message, "Ascon.Pilot.SDK.NotificationsSample");
                return;
            }

            if (value.ChangeKind == NotificationKind.StorageObjectDeleted)
            {
                var loader = new ObjectLoader(_repository);
                var obj = await loader.Load(value.ObjectId, value.ChangesetId());
                var message = string.Format("Storage object {0} has been deleted", obj.DisplayName);
                MessageBox.Show(message, "Ascon.Pilot.SDK.NotificationsSample");
            }

            if (value.ChangeKind == NotificationKind.StorageObjectRenamed)
            {
                var loader = new ObjectLoader(_repository);
                var obj = await loader.Load(value.ObjectId, value.ChangesetId());
                var message = string.Format("Storage object renamed to {0}", obj.DisplayName);
                MessageBox.Show(message, "Ascon.Pilot.SDK.NotificationsSample");
            }
        }

        public void OnError(Exception error)
        {
            
        }

        public void OnCompleted()
        {
            
        }
    }
}
