﻿using System.ComponentModel.Composition;
using System.Text;
using System.Windows;

namespace Ascon.Pilot.SDK.NotificationsSample
{
    [Export(typeof(INotificationsHandler))]
    public class NotificationsHandler : INotificationsHandler
    {
        private readonly IObjectsRepository _repository;

        [ImportingConstructor]
        public NotificationsHandler(IObjectsRepository repository)
        {
            _repository = repository;
        }

        public bool Handle(INotification notification)
        {
            var stringBuilder = new StringBuilder();
            if (notification.UserId != null)
                stringBuilder.Append(_repository.GetPerson(notification.UserId.Value).DisplayName);
            stringBuilder.Append($" {notification.GetActionString()}");
            stringBuilder.Append($" {notification.Title}");

            MessageBox.Show(stringBuilder.ToString());
            return true;
        }
    }
}