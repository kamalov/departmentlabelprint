﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Ascon.Pilot.SDK;
using Ascon.Pilot.SDK.Menu;
using Ascon.Pilot.Theme.ColorScheme;
using Ascon.Pilot.Theme.Controls;
using Ascon.Pilot.Theme.Tools;

namespace ControlsProviderSample
{
    [Export(typeof(IMenu<ObjectsViewContext>))]
    public class Main : IMenu<ObjectsViewContext>
    {
        private readonly IChatControlsProvider _controlsProvider;

        [ImportingConstructor]
        public Main(IChatControlsProvider controlsProvider, IPilotDialogService dialogService)
        {
            _controlsProvider = controlsProvider;
            var accentColor = (Color)ColorConverter.ConvertFromString(dialogService.AccentColor);
            ColorScheme.Initialize(accentColor, dialogService.Theme);
        }

        public void Build(IMenuBuilder builder, ObjectsViewContext context)
        {
            builder.AddItem("ShowChatsCommand", 0).WithHeader("Show chats");
        }

        public void OnMenuItemClick(string name, ObjectsViewContext context)
        {
            if (name != "ShowChatsCommand")
                return;

            var selectedObject = context.SelectedObjects.FirstOrDefault();
            if (selectedObject == null)
                return;

            var chatControlOptions = _controlsProvider.NewChatControlOptions()
                .WithShowChatInfo(false)
                .WithShowTabHeaders(false);
            var view = _controlsProvider.GetObjectChatsControl(selectedObject.Id, chatControlOptions);

            var controlsView = new Controls
            {
                ChatContentPresenter = {Content = view}
            };

            var dialog = new PureWindow
            {
                Content = controlsView,
                Width = 800,
                Height = 600,
                Owner = LayoutHelper.GetOwnerWindow(),
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };
            dialog.ShowDialog();

        }
    }
}
